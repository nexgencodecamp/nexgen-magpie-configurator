/******************************************************************
   LogoCanvas, is the Canvas render class for the rotating logo
 ******************************************************************/

class LogoCanvas extends Canvas {
  
  float ry;
  PShape logo;
  color logoColor = color(203, 197, 150);    //  Nexgen Gold

  public void setup(PGraphics pg) {
    noFill();
    logo = createShape();
    strokeWeight(4.0);
    logo.setStroke(logoColor);
    logo.beginShape();
    logo.vertex(0, 142, 0);
    logo.vertex(-80, 71, 0);
    logo.vertex(-50, 0, 0);
    logo.vertex(50, 0, 0);
    logo.vertex(80, 71, 0);
    logo.vertex(0, 142, 0);
    logo.vertex(50, 0, 0);
    logo.vertex(80, 71, 0);
    logo.vertex(-80, 71, 0);
    logo.vertex(-80, 71, 80);
    logo.vertex(-50, 0, 80);
    logo.vertex(-50, 0, 0);
    logo.vertex(50, 0, 0);
    logo.vertex(50, 0, 80);
    logo.vertex(-50, 0, 80);
    logo.vertex(-80, 71, 80);
    logo.vertex(0, 142, 80);
    logo.vertex(50, 0, 80);
    logo.vertex(80, 71, 80);
    logo.vertex(80, 71, 0);
    logo.vertex(0, 142, 0);
    logo.vertex(0, 142, 80);
    logo.vertex(80, 71, 80);
    logo.vertex(-80, 71, 80);
    logo.endShape();
  }  

  public void draw(PGraphics pg) {
    pushMatrix();
    translate(width, height, -200);
    rotateX(radians(40));
    rotateZ(PI);
    rotateY(ry);
    shape(logo);
    ry += 0.02;
    popMatrix();
  }
  
}
