/******************************************************************
 IMU Data Streaming Options - to compare efficacy of filters
 ******************************************************************/
 
public enum IMUStreamOptions { 
  NOT_STREAMING(0),          //  Default option - used during flight
  CALIBRATE(1),              //  Data for Calibration Screen
  TEST(2),                   //  Data for Test Screen
  RAW(3),                    //  Raw IMU data
  FUSION(4);                 //  Raw and Sensor Fusion Data

  private int value;
  private String[] optionName = {"Not Streaming", "Classic", "Madgwick", "Mahony", "Quaternion", "Fusion"};
  private String[] apiCode = {"<X>", "<J>", "<K>", "<J>", "<I>", "<I>"};

  private IMUStreamOptions(int value) {
      this.value = value;
  }
  
  public boolean isRAW() {
    //if (this == RAW) return true;
    
    return false;
  }
  
  public boolean multipleDataSets() {
    if (this.value > 1) return true;
    
    return false;
  }

  public int getValue() {
      return value;
  }
  
  public String nameAsString() {
    return optionName[this.value];
  }
  
  public String apiCodeString() {
    return apiCode[this.value];
  }
  
  public static IMUStreamOptions valueOfInt(int value) {
    for (IMUStreamOptions s : values()) {
      if (s.value == value) {
        return s;
      }
    }
    return null;
  }
};

 
