/******************************************************************
   IMUCanvas, is the Canvas render class for the IMU Axis
 ******************************************************************/

class IMUCanvas extends Canvas {

  int x; 
  int y; 
  int z; 
  boolean mag, acc;
  PShape imu, chip, dot;

  public void setup(PGraphics pg) {
    imu = createShape(GROUP);
    chip = createShape();
    dot = createShape(ELLIPSE, 18, -4, 5, 3);
    
    dot.setFill(color(255));
    stroke(cp5white);
    
    chip.beginShape();
    chip.noFill();
    chip.strokeWeight(1);
    chip.vertex(0, 0, 0);
    chip.vertex(0, 10, 0);
    chip.vertex(40, 20, 0);
    chip.vertex(40, 10, 0);
    chip.vertex(0, 0, 0);
    chip.vertex(40, -20, 0);
    chip.vertex(80, -10, 0);
    chip.vertex(40, 10, 0);
    chip.vertex(40, 20, 0);
    chip.vertex(80, 0, 0);
    chip.vertex(80, -10, 0);
    chip.endShape();
    
    imu.addChild(chip);
    imu.addChild(dot);
  }  

  public void draw(PGraphics pg) {
    pushMatrix();
    
    fill(cp5red);
    stroke(cp5red);
    strokeWeight(3);
    
    if (acc) {
      text("-Z", x+18, y-40);
    }
    else {
      text("+Z", x+16, y-40);
    }
    
    line(x+40, y-5, x+40, y-49);  //  Z-Axis
    line(x+40, y-50, x+35, y-40);
    line(x+40, y-50, x+45, y-40);
    
    fill(cp5cyan);
    stroke(cp5cyan);
    text("+Y", x+85, y+25);
    line(x+40, y-5, x+90, y+11);  //  Y-Axis
    line(x+90, y+11, x+80, y+2);
    line(x+90, y+11, x+75, y+12);
    
    fill(cp5green);
    stroke(cp5green);
    if (mag) {
      text("+X", x-20, y+35);
      line(x+40, y-5, x-10, y+20);  //  X-Axis
      line(x-10, y+20, x-5, y+10);
      line(x-10, y+20, x+5, y+20);
    }
    else {
      text("+X", x+85, y-40);
      line(x+40, y-5, x+90, y-30);  //  X-Axis
      line(x+90, y-30, x+78, y-30);
      line(x+90, y-30, x+85, y-20);
    }
    
    fill(WHITE);
    stroke(WHITE);
    strokeWeight(1);
    shape(imu, x, y);
    popMatrix();
  }
  
}
