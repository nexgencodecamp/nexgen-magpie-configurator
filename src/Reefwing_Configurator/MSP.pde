/******************************************************************
 Multiwii Serial Protocol
 ******************************************************************/
 
private static final String MSP_HEADER = "$M<";  //  MSP Version 1.0

// Request ID's below 50 are Betaflight specific
// Request/Command ID's between 50 and 100 are Nexgen specific

private static final int
  MSP_API_VERSION          =1,    
  MSP_FC_VARIANT           =2,    
  MSP_FC_VERSION           =3,    
  MSP_BOARD_INFO           =4,    
  MSP_BUILD_INFO           =5,    
  MSP_NAME                 =10,   
  MSP_SET_NAME             =11,
  
  MSP_IMU_ODR              =50,
  MSP_IMU_BIAS             =51,
  
  MSP_IMU_CALIBRATION      =75,
  MSP_SET_ARM              =76,   
  MSP_SET_DISARM           =77, 
  MSP_STOP_STREAM          =78,   
  MSP_CONFIG_CLOSE         =79,   
  
  MSP_IDENT                =100,
  MSP_STATUS               =101,
  MSP_RAW_IMU              =102,
  MSP_SERVO                =103,
  MSP_MOTOR                =104,
  MSP_RC                   =105,
  MSP_RAW_GPS              =106,
  MSP_COMP_GPS             =107,
  MSP_ATTITUDE             =108,
  MSP_ALTITUDE             =109,
  MSP_ANALOG               =110,
  MSP_RC_TUNING            =111,
  MSP_PID                  =112,
  MSP_BOX                  =113,
  MSP_MISC                 =114,
  MSP_MOTOR_PINS           =115,
  MSP_BOXNAMES             =116,
  MSP_PIDNAMES             =117,
  MSP_SERVO_CONF           =120,
    
  
  MSP_SET_RAW_RC           =200,
  MSP_SET_RAW_GPS          =201,
  MSP_SET_PID              =202,
  MSP_SET_BOX              =203,
  MSP_SET_RC_TUNING        =204,
  MSP_ACC_CALIBRATION      =205,
  MSP_MAG_CALIBRATION      =206,
  MSP_SET_MISC             =207,
  MSP_RESET_CONF           =208,
  MSP_SELECT_SETTING       =210,
  MSP_SET_HEAD             =211, // Not used
  MSP_SET_SERVO_CONF       =212,
  MSP_SET_MOTOR            =214,
  
  
  MSP_BIND                 =241,

  MSP_EEPROM_WRITE         =250,
  
  MSP_DEBUGMSG             =253,
  MSP_DEBUG                =254
;

public enum CommandID {
  MSP_API_VERSION(1),    
  MSP_FC_VARIANT(2),    
  MSP_FC_VERSION(3),    
  MSP_BOARD_INFO(4),    
  MSP_BUILD_INFO(5),    
  MSP_NAME(10),   
  MSP_SET_NAME(11), 
  MSP_IMU_ODR(50),
  MSP_IMU_BIAS(51),
  MSP_IMU_CALIBRATION(75),
  MSP_SET_ARM(76),   
  MSP_SET_DISARM(77),  
  MSP_IDENT(100),
  MSP_STATUS(101),
  MSP_RAW_IMU(102),
  MSP_SERVO(103),
  MSP_MOTOR(104),
  MSP_RC(105),
  MSP_RAW_GPS(106),
  MSP_COMP_GPS(107),
  MSP_ATTITUDE(108),
  MSP_ALTITUDE(109),
  MSP_ANALOG(110),
  MSP_RC_TUNING(111),
  MSP_PID(112),
  MSP_BOX(113),
  MSP_MISC(114),
  MSP_MOTOR_PINS(115),
  MSP_BOXNAMES(116),
  MSP_PIDNAMES(117),
  MSP_SERVO_CONF(120)
  ;
  
  public final int code;
  
  private CommandID(int code) {
    this.code = code;
  }
  
  public static CommandID valueOfCode(int code) {
    for (CommandID c : values()) {
      if (c.code == code) {
        return c;
      }
    }
    return null;
  }
  
};

/******************************************************************
 Alias for multiTypes (MultiWiiConf)
 ******************************************************************/

final int TRI           =1;
final int QUADP         =2;
final int QUADX         =3;  //  Nexgen Magpie QuadX
final int BI            =4;
final int GIMBAL        =5;
final int Y6            =6;
final int HEX6          =7;
final int FLYING_WING   =8;
final int Y4            =9;
final int HEX6X         =10;
final int OCTOX8        =11;
final int OCTOFLATX     =12;
final int OCTOFLATP     =13;
final int AIRPLANE      =14;
final int HELI_120_CCPM =15;
final int HELI_90_DEG   =16;
final int VTAIL4        =17;
final int HEX6H         =18;
final int PPM_TO_SERVO  =19;
final int DUALCOPTER    =20;
final int SINGLECOPTER  =21;

/******************************************************************
 Serial Message State Machine & Globals (MultiWiiConf)
 ******************************************************************/

public static final int
  IDLE = 0,
  HEADER_START = 1,
  HEADER_M = 2,
  HEADER_ARROW = 3,
  HEADER_SIZE = 4,
  HEADER_CMD = 5,
  HEADER_ERR = 6
;

int c_state = IDLE;
boolean err_rcvd = false;

byte checksum=0;
byte cmd;
int offset=0, dataSize=0;
byte[] inBuf = new byte[256];


int p;
int read32() {return (inBuf[p++]&0xff) + ((inBuf[p++]&0xff)<<8) + ((inBuf[p++]&0xff)<<16) + ((inBuf[p++]&0xff)<<24); }
int read16() {return (inBuf[p++]&0xff) + ((inBuf[p++])<<8); }
int read8()  {return  inBuf[p++]&0xff;}

String readStr() {
  String raw = new String(inBuf, StandardCharsets.UTF_8);
  
  Arrays.fill(inBuf, (byte)0);  //  Clear Input buffer
  return raw.replaceAll("\\P{Print}", "");
}

String[] readMultiStr() {
  String raw = new String(inBuf, StandardCharsets.UTF_8);
  
  Arrays.fill(inBuf, (byte)0);  //  Clear Input buffer
  // logConfigurator("> Raw String: " + raw);
  return raw.replaceAll("\0+", "\0").split("\0", -1);
}
        
/******************************************************************
 MSP Protocol Functions (MultiWiiConf)
 ******************************************************************/

//send msp without payload
private List<Byte> requestMSP(int msp) {
  return  requestMSP( msp, null);
}

//send multiple msp without payload
private List<Byte> requestMSP (int[] msps) {
  List<Byte> s = new LinkedList<Byte>();
  for (int m : msps) {
    s.addAll(requestMSP(m, null));
  }
  return s;
}

//send msp with payload
private List<Byte> requestMSP (int msp, Character[] payload) {
  if(msp < 0) {
   return null;
  }
  List<Byte> bf = new LinkedList<Byte>();
  for (byte c : MSP_HEADER.getBytes()) {
    bf.add( c );
  }
  
  byte checksum=0;
  byte pl_size = (byte)((payload != null ? int(payload.length) : 0)&0xFF);
  bf.add(pl_size);
  checksum ^= (pl_size&0xFF);
  
  bf.add((byte)(msp & 0xFF));
  checksum ^= (msp&0xFF);
  
  if (payload != null) {
    for (char c :payload){
      bf.add((byte)(c&0xFF));
      checksum ^= (c&0xFF);
    }
  }
  bf.add(checksum);
  return (bf);
}

void sendRequestMSP(List<Byte> msp) {
  byte[] arr = new byte[msp.size()];
  int i = 0;
  for (byte b: msp) {
    arr[i++] = b;
  }
  serialPort.write(arr); // send the complete byte sequence in one go
}

/******************************************************************
 Evaluate MSP Responses (MultiWiiConf)
 ******************************************************************/

public void evaluateCommand(byte cmd, int dataSize) {
  int i, protocolVersion, multiType, multiCapability;
  int minor, major, patch;
  int i2cError, sensor = 0, configSetting;
  int gx, gy, gz, ax, ay, az, mx, my, mz;
  int icmd = (int)(cmd&0xFF);
  
  switch(icmd) {
    case MSP_IDENT:
      protocolVersion = read8();
      multiType = read8();
      read8(); // MSP version - not used
      multiCapability = read32();// capability
      if (protocolVersion >= 2) { logConfigurator("> MSP Protocol V2"); }
      else { logConfigurator("> MSP Protocol V1"); }
      if (multiType == QUADX) { logConfigurator("> Magpie Drone Connected"); }
      else { logConfigurator("> Unknown Drone Connected"); }
      break;
    case MSP_STATUS:
      loopFrequency = read16();
      i2cError = read16();
      sensor = read16();
      
      int flags = read32();  
      int stateValue = read8();
      
      if (droneState.getValue() != stateValue) {
        droneState = DroneStates.valueOfInt(stateValue);
        logConfigurator("> New Drone state: " + droneState.nameAsString());
      }
      processArmingFlags(flags);
      break;
    case MSP_FC_VERSION:
      major = read8();
      minor = read8();
      patch = read8();
      logConfigurator("> Flight Controller Version: " + major + "." + minor + "." + patch + ".");
      break;
    case MSP_RAW_IMU:
      //  Read data and convert back to a float
      acc_x = float(read16()) / 100.0; acc_y = float(read16()) / 100.0; acc_z = float(read16()) / 100.0;
      gyr_x = float(read16()) / 100.0; gyr_y = float(read16()) / 100.0; gyr_z = float(read16()) / 100.0;
      mag_x = float(read16()) / 100.0; mag_y = float(read16()) / 100.0; mag_z = float(read16()) / 100.0;
      processIMUdata();
      break;
    case MSP_ATTITUDE:
      roll = float(read16()) / 100.0;
      pitch = float(read16()) / 100.0;
      yaw = float(read16()) / 100.0;
      break;
    case MSP_FC_VARIANT:
      String variant = readStr();
      
      logConfigurator("> MSP Variant = " + variant);
      break;
    case MSP_RC:
      channels[0] = read16(); channels[1] = read16(); channels[2] = read16();
      channels[3] = read16(); channels[4] = read16(); channels[5] = read16();
      channels[6] = read16(); channels[7] = read16(); channels[8] = read16();
      channels[9] = read16(); channels[10] = read16();
      read16(); read16(); read16(); read16(); read16();  //  flush unused channels
      break;
    case MSP_ANALOG:
      int vBat, mAhDrawn, amperage, tempRSSI;
      
      vBat = read16();
      mAhDrawn = read16();
      tempRSSI = read16();
      amperage = read16();  //  Used to transmit Rexceiver Voltage, rxVoltage
      
      logConfigurator("> Drone Battery Payload: " + vBat);
      droneBattery = float(vBat) / 100.0;
      rssi = int((float(tempRSSI)/1811.0) * 100.0);
      rxVoltage = (float(amperage)/1811.0) * 5.0;
      break;
    case MSP_IMU_ODR:
      gyroSampleRate = float(read16());
      accelSampleRate = float(read16());
      magSampleRate = float(read16());
      break;
    case MSP_IMU_BIAS:
      String[] biasArray = readMultiStr();
      
      gx_bias = biasArray[0]; gy_bias = biasArray[1]; gz_bias = biasArray[2];
      ax_bias = biasArray[3]; ay_bias = biasArray[4]; az_bias = biasArray[5];
      mx_bias = biasArray[6]; my_bias = biasArray[7]; mz_bias = biasArray[8];
      break;
    case MSP_DEBUGMSG:
      String msg = readStr();
      
      logConfigurator("> DEBUG: " + msg);
      break;
    default:
      logConfigurator("! Unhandled MSP MSG Response: " + icmd);
  }
}

public void processIMUdata() {
  imuSampleCount++;
        
  if (firstIMUread) {
    gyr_min_x = gyr_x;
    gyr_min_y = gyr_y;
    gyr_min_z = gyr_z;
    acc_min_x = acc_x;
    acc_min_y = acc_y;
    acc_min_z = acc_z;
    mag_min_x = mag_x;
    mag_min_y = mag_y;
    mag_min_z = mag_z;
    
    gyr_avg_x = gyr_x;
    gyr_avg_y = gyr_y;
    gyr_avg_z = gyr_z;
    acc_avg_x = acc_x;
    acc_avg_y = acc_y;
    acc_avg_z = acc_z;
    mag_avg_x = mag_x;
    mag_avg_y = mag_y;
    mag_avg_z = mag_z;
    
    gyr_max_x = gyr_x;
    gyr_max_y = gyr_y;
    gyr_max_z = gyr_z;
    acc_max_x = acc_x;
    acc_max_y = acc_y;
    acc_max_z = acc_z;
    mag_max_x = mag_x;
    mag_max_y = mag_y;
    mag_max_z = mag_z;
    
    firstIMUread = false;
  }
  else {
    gyr_min_x = min(gyr_min_x, gyr_x);
    gyr_min_y = min(gyr_min_y, gyr_y);
    gyr_min_z = min(gyr_min_z, gyr_z);
    acc_min_x = min(acc_min_x, acc_x);
    acc_min_y = min(acc_min_y, acc_y);
    acc_min_z = min(acc_min_z, acc_z);
    mag_min_x = min(mag_min_x, mag_x);
    mag_min_y = min(mag_min_y, mag_y);
    mag_min_z = min(mag_min_z, mag_z);
    
    gyr_avg_x = (gyr_avg_x + gyr_x) / float(imuSampleCount);
    gyr_avg_y = (gyr_avg_y + gyr_y) / float(imuSampleCount);
    gyr_avg_z = (gyr_avg_z + gyr_z) / float(imuSampleCount);
    acc_avg_x = (acc_avg_x + acc_x) / float(imuSampleCount);
    acc_avg_y = (acc_avg_y + acc_y) / float(imuSampleCount);
    acc_avg_z = (acc_avg_z + acc_z) / float(imuSampleCount);
    mag_avg_x = (mag_avg_x + mag_x) / float(imuSampleCount);
    mag_avg_y = (mag_avg_y + mag_y) / float(imuSampleCount);
    mag_avg_z = (mag_avg_z + mag_z) / float(imuSampleCount);
  
    gyr_max_x = max(gyr_max_x, gyr_x);
    gyr_max_y = max(gyr_max_y, gyr_y);
    gyr_max_z = max(gyr_max_z, gyr_z);
    acc_max_x = max(acc_max_x, acc_x);
    acc_max_y = max(acc_max_y, acc_y);
    acc_max_z = max(acc_max_z, acc_z);
    mag_max_x = max(mag_max_x, mag_x);
    mag_max_y = max(mag_max_y, mag_y);
    mag_max_z = max(mag_max_z, mag_z);
    
    //  logConfigurator("Nexgen: Gyro DEBUG - gyr_avg_x: " + gyr_avg_x + " gyr_x: " + gyr_x + " imuSampleCount: " + imuSampleCount);
    
    if (imuSampleCount >= maxIMUsamples) { 
      gyr_avg_x = 0.0;
      gyr_avg_y = 0.0;
      gyr_avg_z = 0.0;
      acc_avg_x = 0.0;
      acc_avg_y = 0.0;
      acc_avg_z = 0.0;
      mag_avg_x = 0.0;
      mag_avg_y = 0.0;
      mag_avg_z = 0.0;
      
      imuSampleCount = 0; 
    }
  }
  
}
