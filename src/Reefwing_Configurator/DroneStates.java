/******************************************************************
 State Machine for Drone
 
  LED State Indication  - DISCONNECTED    Blinking Blue, Solid Yellow
                        - USB_SERIAL      Solid Magenta, Breathing Yellow
                        - SBUS            Blinking Green, Solid Yellow
                        - USB_SBUS        Solid Cyan, Breathing Yellow
                        - FATAL_ERROR     Blinking Red, Blinking Yellow
                        - ARMED           Solid Green, Yellow Off
                        - UNDEFINED       Off, Off
                        
 ******************************************************************/

public enum DroneStates { 
  DISCONNECTED(0),     //  Startup state - nothing connected
  USB_SERIAL(1),       //  PC Connected
  SBUS(2),             //  Radio Control SBUS connected
  USB_SBUS(3),         //  USB and SBUS connected
  ARMED(4),            //  Ready to Fly
  FATAL_ERROR(5),      //  Big Problem!
  UNDEFINED(6);        //  Used for Previous State
  
  private int value;
  private String[] stateName = {"DISCONNECTED", "USB_SERIAL", "SBUS", "USB_SBUS", "ARMED", "FATAL_ERROR", "UNDEFINED"};

  private DroneStates(int value) {
      this.value = value;
  }

  public int getValue() {
      return value;
  }
  
  public String nameAsString() {
    return stateName[this.value];
  }
  
  public static DroneStates valueOfInt(int value) {
    for (DroneStates s : values()) {
      if (s.value == value) {
        return s;
      }
    }
    return null;
  }
};
