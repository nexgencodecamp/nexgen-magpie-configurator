/******************************************************************
 Console Logging Helper Functions
 ******************************************************************/
 
 void logDrone(String msg) {
   consoleText.append(msg + "\n");
   consoleText.scroll(5.0);
 }
 
 void logConfigurator(String msg) {
   consoleText.append(msg + "\n");
   consoleText.scroll(5.0);
 }
 
 void logError(String msg) {
   consoleText.append(msg + "\n");
   consoleText.scroll(5.0);
 }
 
 void logHelp(String msg) {
   consoleText.append(msg + "\n");
   consoleText.scroll(5.0);
 }
