/******************************************************************
 Nexgen Configurator Keyboard Commands
 ******************************************************************/

public enum ControlStates {  
    INC_PITCH('u'), //  Arrow UP
    DEC_PITCH('n'), //  Arrow DOWN
    INC_ROLL('l'), //  Arrow LEFT
    DEC_ROLL('r'), //  Arrow RIGHT
    INC_THROTTLE('w'), 
    DEC_THROTTLE('s'), 
    INC_YAW('a'), 
    DEC_YAW('d'), 
    BATTERY('b'),
    LOOP_FREQ('f'),
    VERSION('v'), 
    RESET(' '), 
    HELP('h'),
    IMU_NONE('0'), 
    IMU_ROLL('1'), 
    IMU_PITCH('2'), 
    IMU_YAW('3'), 
    NOP('x')
    ;

  public final char code;

  private ControlStates(char code) {
    this.code = code;
  }

  public static ControlStates valueOfCode(char code) {
    for (ControlStates c : values()) {
      if (c.code == code) {
        return c;
      }
    }
    return null;
  }
};
