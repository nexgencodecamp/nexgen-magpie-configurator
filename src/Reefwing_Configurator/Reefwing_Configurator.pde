/******************************************************************
 Nexgen_Configurator
 
 @file    Nexgen_Configurator.pde
 @brief   Used to setup the Nexgen drones (Magpie and Lark)
 @author  David Such
 
 Code:        David Such
 Version:     1.6
 Date:        
 
 1.0  Original Release              07/12/20
 1.1  PID Tuning & Mac Big Sur fix  22/06/21
 1.2  IMU Calibration               07/11/21
 1.3  IMU Testing                   01/01/22
 1.4  IMU and Filters               31/03/22
 1.5  MSP replacing NSP             04/05/22
 1.6
 
 ******************************************************************
 
 Notes:
 
 1. You will need to select the serial port that your drone is connected to.
 2. Requires installation of the controlP5 library v2.2.6
 
 ******************************************************************
 
 SBUS Channels  - Throttle:     rc.channelValue[0]
                - Aileron:      rc.channelValue[1] (Roll)
                - Elevator:     rc.channelValue[2] (Pitch)
                - Rudder:       rc.channelValue[3] (Yaw)
                - BUZZER        rc.channelValue[4]
                - RSSI:         rc.channelValue[5]
                - Rx Voltage:   rc.channelValue[6]
                - ARM / DISARM  rc.channelValue[7]
                - FLIGHT MODE   rc.channelValue[8]
                
                - Fail Safe     rc.channelValue[9]
                - Lost Frames   rc.channelValue[10]
                
 ******************************************************************
                
  Arm Prevention Flags (1 Byte):

  BIT             7       6       5       4       3       2       1       0
  DESCRIPTION     Config  Low     Boot    Angle   Thrott  RxLoss  Fail    No
                  (USB)   Bat                     > 0     (SBUS)  Safe    Gyro 
         
 ******************************************************************
                  
  LED State Indication  - DISCONNECTED    Blinking Blue, Solid Yellow
                        - USB_SERIAL      Solid Magenta, Breathing Yellow
                        - SBUS            Blinking Green, Solid Yellow
                        - USB_SBUS        Solid Cyan, Breathing Yellow
                        - FATAL_ERROR     Blinking Red, Blinking Yellow
                        - ARMED           Solid Green, Yellow Off
 
 ******************************************************************/

import processing.serial.*;
import controlP5.*;
import java.util.*;  //  Required for List type
import java.nio.charset.StandardCharsets;  //  For converting the serial buffer to a string

/******************************************************************
 CONSTANTS - COLOUR PALETTE
 ******************************************************************/

final color cp5black = color(0, 0, 0);
final color cp5white = color(255, 255, 255);
final color cp5red = color(255, 0, 0);
final color cp5green = color(0, 255, 0);
final color cp5orange = color(255, 165, 0);
final color cp5yellow = color(255, 255, 0);
final color cp5blue = color(0, 44, 91);
final color cp5cyan = color(0, 255, 255);
final color cp5magenta = color(255, 0, 255);
final color cp5grey = color(120);
final color cp5brown = color(165, 42, 42);

final color lightBlue = color(0, 0, 255);
final color darkGrey = color(43, 45, 47);
final color darkGreen = color(21, 71, 52);
final color nexgenGold = color(203, 197, 150);
final color nexgenGreen = color(171, 253, 4);
final color nexgenAqua = color(0, 255, 196);

final color ROLL_FILTER_COLOUR = cp5red;
final color PITCH_FILTER_COLOUR = cp5magenta;
final color YAW_FILTER_COLOUR = cp5brown;

/******************************************************************
 CONSTANTS - GENERAL
 ******************************************************************/

final int BAUD_RATE = 115200;
final int MIN_PULSEWIDTH = 1000;
final int MAX_PULSEWIDTH = 2000;
final int MIN_ASCII = 33;
final int MAX_ASCII = 122;
final int SWITCH_ON = 991;
final int CLEAR = 99;
final int BLACK = 0;
final int WHITE = 255;
final int X_AXIS = 1;
final int Y_AXIS = 2;
final int GYRO_RANGE = 300;
final int ACCEL_RANGE = 4;
final int MAG_RANGE = 400;
final char START_BYTE = '<';
final char STOP_BYTE = '>';
final float VERSION = 1.6;
final float FULLY_CHARGED = 8.4;
final String NONE = "00000000";
final String[] DRONE_NAMES = {"MAGPIE", "LARK"};
final String[] IMU_NAMES = {"LSM9DS1", "MPU6500"};
final String[] DRONE_STATES = {"DISCONNECTED", "USB SERIAL", "RADIO CONTROL - SBUS", "USB SERIAL & RADIO CONTROL", "ARMED", "FATAL ERROR - SHUTDOWN"};
final String[] IMU_DATA = {"Classic Complementary Filter", "Madgwick Filter", "Mahony Filter", "Quaternion Complementary Filter", "Sensor Fusion Filter"};

/******************************************************************
 CONTROL P5 OBJECTS
 ******************************************************************/

ControlP5 cp5mainMenu, cp5console, cp5testIMU, cp5calibrateIMU, cp5radioControl, cp5testMotors, cp5calibrateESC, cp5tunePID;
ControlWindow controlWindow;
Canvas cc;
DroneCanvas dc, mc;
IMUCanvas fakeC, gyroC, accelC, magC;
Println console;
Button discButton, nextButton;
CheckBox imuSelect;
Chart imuChart;
Toggle armTestToggle, armCalibrateToggle;
Knob masterTestThrottle, masterCalibrateThrottle;
Slider m1dutyCycle, m2dutyCycle, m3dutyCycle, m4dutyCycle;
Slider imuScale;
ScrollableList droneTypeList, imuDataList, imuTypeList;

/******************************************************************
 PUBLIC ENUMS
 ******************************************************************/
 
public enum IMUType { 
  LSM9DS1, MPU6500
};

public enum AppState { 
  MAIN_MENU, TEST_IMU, CALIBRATE_IMU, RADIO_CONTROL, CALIBRATE_ESC, TEST_MOTORS, TUNE_PID, MAX_STATES
};

public enum CalibrationState { 
  START, CONNECT, ARM, MAX_THROTTLE, MIN_THROTTLE, DONE, MAX_STATES
};

/******************************************************************
 STATE MACHINE / ENUM VARIABLES
 ******************************************************************/

AppState state = AppState.MAIN_MENU;
ControlStates controlState = ControlStates.NOP;
DroneStates droneState = DroneStates.DISCONNECTED;
CalibrationState calibrationState = CalibrationState.START;
IMUStreamOptions imuStreamOption = IMUStreamOptions.NOT_STREAMING;
IMUType selectedIMU = IMUType.LSM9DS1;
CommandID mspRequest = CommandID.MSP_IDENT;

/******************************************************************
 GLOBAL VARIABLES
 ******************************************************************/

Serial serialPort = null;
String portName = null;
ScrollableList serialPortsList;
Textarea consoleText;
String serialString = null;
PFont bold, smallBold, regular;
float throttle = 0.0;
float roll = 0.0, pitch = 0.0, yaw = 0.0;  //  Filtered Values
float gyr_x = 0.0, gyr_y = 0.0, gyr_z = 0.0;
float gyr_min_x = 0.0, gyr_min_y = 0.0, gyr_min_z = 0.0;
float gyr_avg_x = 0.0, gyr_avg_y = 0.0, gyr_avg_z = 0.0;
float gyr_max_x = 0.0, gyr_max_y = 0.0, gyr_max_z = 0.0;
float gyrRollAngle = 0.0, gyrPitchAngle = 0.0, gyrYawAngle = 0.0;
float acc_x = 0.0, acc_y = 0.0, acc_z = 0.0;
float acc_min_x = 0.0, acc_min_y = 0.0, acc_min_z = 0.0;
float acc_avg_x = 0.0, acc_avg_y = 0.0, acc_avg_z = 0.0;
float acc_max_x = 0.0, acc_max_y = 0.0, acc_max_z = 0.0;
float accRollAngle = 0.0, accPitchAngle = 0.0;
float mag_x = 0.0, mag_y = 0.0, mag_z = 0.0;
float mag_min_x = 0.0, mag_min_y = 0.0, mag_min_z = 0.0;
float mag_avg_x = 0.0, mag_avg_y = 0.0, mag_avg_z = 0.0;
float mag_max_x = 0.0, mag_max_y = 0.0, mag_max_z = 0.0;
float magYawAngle = 0.0;
float gyroSampleRate = 119.0, accelSampleRate = 119.0, magSampleRate = 40.0;
String gx_bias = "0.0", gy_bias = "0.0", gz_bias = "0.0";
String ax_bias = "0.0", ay_bias = "0.0", az_bias = "0.0";
String mx_bias = "0.0", my_bias = "0.0", mz_bias = "0.0";
float droneBattery = 0.0;
float rxVoltage = 0.0;
int time = millis();
int rssi = 0;
int loopFrequency = 0;
int imuSampleCount = 0, maxIMUsamples = 10;
int armPreventionFlags = 0;
int[] channels = new int [16];    //  SBUS Channels (TAER, Buzzer, RSSI, RxBAT, Arm/Disarm, Flight Mode, Fail Safe, Lost Frames)
String[] channelNames = {"Throttle", "Roll", "Pitch", "Yaw", "Buzzer", "RSSI", "Rx Voltage", "Armed", "Flight Mode"};
boolean enterMotors = true, sbusAvailable = false, firstIMUread = true;
PImage cascadedPID, magpieDrone;
String subMenu = "";

/******************************************************************
 PID Parameters
 ******************************************************************/

float rollKp = 4.0, rollKi = 0.03, rollKd = 17;
float pitchKp = 4.0, pitchKi = 0.03, pitchKd = 17;
float yawKp = 8.5, yawKi = 0.0, yawKd = 0.0;
float rollPitchRate = 0.0, yawRate = 0.0;

/******************************************************************
 ARM Prevention Flags & BIT Mask Constants
 ******************************************************************/

boolean configFlag = false, lowBatFlag = false, bootFlag = false;
boolean angleFlag = false, throttleFlag = false, rxLossFlag = false;
boolean failSafeFlag = false, noGyroFlag = false;

final  int NO_GYRO_MASK    = 0b00000001;
final  int FAILSAFE_MASK   = 0b00000010;
final  int RX_LOSS_MASK    = 0b00000100;
final  int THROTTLE_MASK   = 0b00001000;
final  int ANGLE_MASK      = 0b00010000;
final  int BOOT_MASK       = 0b00100000;
final  int LOW_BAT_MASK    = 0b01000000;
final  int CONFIG_MASK     = 0b10000000;

/******************************************************************
 SETUP
 ******************************************************************/

void setup() 
{
  /******************************************************************
   Application Screens
   ******************************************************************/

  cp5mainMenu     = new ControlP5(this);
  cp5console      = new ControlP5(this);
  cp5radioControl = new ControlP5(this);
  cp5testIMU      = new ControlP5(this);
  cp5calibrateIMU = new ControlP5(this);
  cp5calibrateESC = new ControlP5(this);
  cp5testMotors   = new ControlP5(this);
  cp5tunePID      = new ControlP5(this);

  /******************************************************************
   Font Management
   ******************************************************************/

  bold = createFont("Arial-BoldMT", 24);
  smallBold = createFont("Arial-BoldMT", 14);
  regular = createFont("Lucida Sans Regular", 14);
  ControlFont cf1 = new ControlFont(createFont("Arial", 16));

  cp5mainMenu.setFont(cf1);
  cp5console.setFont(cf1);
  cp5radioControl.setFont(cf1);
  cp5testIMU.setFont(cf1);
  cp5calibrateIMU.setFont(cf1);
  cp5calibrateESC.setFont(cf1);
  cp5testMotors.setFont(cf1);
  cp5tunePID.setFont(cf1);

  /******************************************************************
   SBUS Channel default values
   ******************************************************************/

  for (int c = 1; c < 4; c++) {
    channels[c] = 992;  //  Roll, Pitch, Yaw = 0 degrees
  }

  channels[0] = 172;  // Throttle
  channels[4] = 0;    // Buzzer 
  channels[5] = 0;    // RSSI
  channels[6] = 0;    // Rx Voltage
  channels[7] = 0;    // ARM/DISARM
  channels[8] = 0;    // Flight Mode
  channels[9] = 0;    // Fail Safe
  channels[10] = 0;   // Lost Frames

  /******************************************************************
   CP5 - Add Main Menu Buttons
   ******************************************************************/
   
  magpieDrone = loadImage("MagpieDrone_DS3.png");
  magpieDrone.resize(350, 350);

  cp5mainMenu.addButton("calibrateIMU")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(40, 100)
    .setSize(150, 40)
    .setCaptionLabel("Calibrate IMU")
    .setBroadcast(true)
    ;

  cp5mainMenu.addButton("testIMU")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(40, 160)
    .setSize(150, 40)
    .setCaptionLabel("Test IMU")
    .setBroadcast(true)
    ;

  cp5mainMenu.addButton("radioControl")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(40, 220)
    .setSize(150, 40)
    .setCaptionLabel("Radio Control")
    .setBroadcast(true)
    ;

  cp5mainMenu.addButton("calibrateESC")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(40, 280)
    .setSize(150, 40)
    .setCaptionLabel("Calibrate ESC")
    .setBroadcast(true)
    ;

  cp5mainMenu.addButton("testMotors")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(40, 340)
    .setSize(150, 40)
    .setCaptionLabel("Test Motors")
    .setBroadcast(true)
    ;
    
  cp5mainMenu.addButton("tunePID")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(40, 400)
    .setSize(150, 40)
    .setCaptionLabel("Tune PID")
    .setBroadcast(true)
    ;
    
  cp5mainMenu.addButton("requestLoopFreq")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(545, 605)
    .setSize(90, 25)
    .setCaptionLabel("Request")
    .setColorBackground(darkGrey)
    .setBroadcast(true)
    ;
    
  cp5mainMenu.addButton("configDisarm")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(40, 650)
    .setSize(90, 25)
    .setCaptionLabel("DISARM")
    .setColorBackground(darkGrey)
    .setBroadcast(true)
    ;

  droneTypeList = cp5mainMenu.addScrollableList("droneType")
    .setCaptionLabel("Drone Type")
    .setPosition(500, 60)
    .setColorValue(nexgenAqua)
    .setColorBackground(darkGrey)
    .setBarHeight(30)
    .setWidth(135)
    .setItemHeight(30)
    .setOpen(false)
    ;

  for (int i = 0; i < DRONE_NAMES.length; i++) 
    droneTypeList.addItem(DRONE_NAMES[i], i);
    
  droneTypeList.setValue(0);

  cc = new LogoCanvas();
  cc.post(); // use cc.pre(); to draw under existing controllers.
  cp5mainMenu.addCanvas(cc); // add the canvas to cp5mainMenu

  /******************************************************************
   CP5 - Create Console
   ******************************************************************/
   
  cp5console.enableShortcuts();

  cp5console.addButton("clearConsole")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 350, 320)
    .setSize(70, 30)
    .setId(CLEAR)
    .setCaptionLabel("Clear")
    .setBroadcast(true)
    ;

  cp5console.addButton("copyConsole")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 260, 320)
    .setSize(70, 30)
    .setCaptionLabel("Copy")
    .setColorBackground(darkGrey)
    .setBroadcast(true)
    ;

  cp5console.addButton("refreshSerial")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 350, 60)
    .setSize(90, 30)
    .setCaptionLabel("Refresh")
    .setBroadcast(true)
    ;

  discButton = cp5console.addButton("disconnectSerial")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 150, 60)
    .setSize(120, 30)
    .setCaptionLabel("Disconnect")
    .setColorBackground(darkGrey)
    .setBroadcast(true)
    ;

  cp5console.addTextfield("console")
    .setSize(width - 430, 35)
    .setCaptionLabel("Console")
    .setPosition(40, height - 80)
    .setFont(createFont("arial", 20))
    .setAutoClear(true)
    ;

  consoleText = cp5console.addTextarea("txt")
    .setPosition(width - 350, 100)
    .setSize(320, 200)
    .setFont(createFont("arial", 12))
    .setLineHeight(14)
    .setColorBackground(darkGrey)
    ;

  console = cp5console.addConsole(consoleText);

  String[] portNames = Serial.list();

  serialPortsList = cp5console.addScrollableList("serialPorts")
    .setCaptionLabel("Serial Ports")
    .setPosition(width - 350, 20)
    .setColorValue(nexgenAqua)
    .setColorBackground(darkGrey)
    .setBarHeight(30)
    .setWidth(320)
    .setItemHeight(30)
    .setOpen(false)
    ;

  for (int i = 0; i < portNames.length; i++) 
    serialPortsList.addItem(portNames[i], i);

  /******************************************************************
   CP5 - Radio Control Screen
   ******************************************************************/

  cp5radioControl.addButton("mainMenu")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 180, height - 80)
    .setSize(150, 40)
    .setCaptionLabel("Main Menu")
    .setBroadcast(true)
    ;
    
  cp5radioControl.addButton("requestLoopFreq")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(545, 605)
    .setSize(90, 25)
    .setCaptionLabel("Request")
    .setColorBackground(darkGrey)
    .setBroadcast(true)
    ;

  dc = new DroneCanvas();
  dc.pre(); // use cc.pre(); to draw under existing controllers.
  cp5radioControl.addCanvas(dc); 
  cp5radioControl.hide();
  
  /******************************************************************
   CP5 - Calibrate IMU Screen
   ******************************************************************/
   
   cp5calibrateIMU.addButton("mainMenu")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 180, height - 80)
    .setSize(150, 40)
    .setCaptionLabel("Main Menu")
    .setBroadcast(true)
    ;
    
  cp5calibrateIMU.addButton("calibrateCode")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 180, height - 130)
    .setSize(150, 40)
    .setCaptionLabel("Calibrate")
    .setColorBackground(darkGreen)
    .setBroadcast(true)
    ;
    
  cp5calibrateIMU.addButton("requestLoopFreq")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(545, 605)
    .setSize(90, 25)
    .setCaptionLabel("Request")
    .setColorBackground(darkGrey)
    .setBroadcast(true)
    ;
    
  imuTypeList = cp5calibrateIMU.addScrollableList("imuType")
    .setCaptionLabel("IMU Type")
    .setPosition(504, 67)
    .setColorValue(nexgenAqua)
    .setColorBackground(darkGrey)
    .setBarHeight(30)
    .setWidth(135)
    .setItemHeight(30)
    .setOpen(false)
    ;

  for (int i = 0; i < IMU_NAMES.length; i++) 
    imuTypeList.addItem(IMU_NAMES[i], i);
    
  imuTypeList.setValue(0);
    
  fakeC = new IMUCanvas();  //  This is a fudge
  fakeC.x = 2000;
  fakeC.y = 2000;
  fakeC.z = 30;
  fakeC.mag = false;
  fakeC.acc = false;
  fakeC.post(); // use gc.pre(); to draw under existing controllers.
  
  gyroC = new IMUCanvas();
  gyroC.x = 530;
  gyroC.y = 165;
  gyroC.z = 30;
  gyroC.mag = false;
  gyroC.acc = false;
  gyroC.post(); // use gc.pre(); to draw under existing controllers.
  
  accelC = new IMUCanvas();
  accelC.x = 530;
  accelC.y = 312;
  accelC.z = 30;
  accelC.mag = false;
  accelC.acc = true;
  accelC.post(); // use gc.pre(); to draw under existing controllers.
  
  magC = new IMUCanvas();
  magC.x = 530;
  magC.y = 459;
  magC.z = 30;
  magC.mag = true;
  magC.acc = false;
  magC.post(); // use gc.pre(); to draw under existing controllers.
  
  cp5calibrateIMU.addCanvas(fakeC);
  cp5calibrateIMU.addCanvas(gyroC); // add the canvas to cp5calibrateIMU 
  cp5calibrateIMU.addCanvas(accelC);
  cp5calibrateIMU.addCanvas(magC);
  cp5calibrateIMU.hide();

  /******************************************************************
   CP5 - Test IMU Screen
   ******************************************************************/

  cp5testIMU.addButton("mainMenu")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 180, height - 80)
    .setSize(150, 40)
    .setCaptionLabel("Main Menu")
    .setBroadcast(true)
    ;
    
  cp5testIMU.addButton("resetIMU")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(535, 646)
    .setSize(95, 25)
    .setCaptionLabel("Reset")
    .setColorBackground(cp5red)
    .setBroadcast(true)
    ;

  imuSelect = cp5testIMU.addCheckBox("selectPitchRollYaw")
    .setPosition(132, 480)
    .setSize(20, 20)
    .setColorBackground(darkGrey)
    .setColorForeground(cp5grey)
    .setColorActive(cp5white)
    .setColorLabel(nexgenAqua)
    .setItemsPerRow(3)
    .setSpacingColumn(115)
    .addItem("Roll / x", 1)
    .addItem("Pitch / y", 2)
    .addItem("Yaw / z", 3)
    .activateAll() 
    ;

  for (Toggle t : imuSelect.getItems()) {
    t.getCaptionLabel().getStyle().moveMargin(-7, 0, 0, -3);
    t.getCaptionLabel().getStyle().movePadding(7, 0, 0, 3);
  }

  imuChart = cp5testIMU.addChart("FUSED DATA")
    .setPosition(40, 100)
    .setSize(600, 200)
    .setRange(-180, 180)
    .setView(Chart.LINE) 
    ;

  imuChart.getColor().setBackground(color(WHITE));
  imuChart.setStrokeWeight(2.0);

  addRollDataset();
  addPitchDataset();
  addYawDataset();
  
  imuScale = cp5testIMU.addSlider("sliderIMUscale")
    .setBroadcast(false)
    .setRange(100, 150)
    .setValue(100)
    .setPosition(542, 600)
    .setSize(80, 15)
    .setNumberOfTickMarks(5)
    .snapToTickMarks(true)
    .setColorValue(0xffffffff)
    .setColorLabel(0xffdddddd)
    .setCaptionLabel("")
    .setLock(false)
    .setBroadcast(true)
    ;
    
  imuDataList = cp5testIMU.addScrollableList("imuData")
    .setCaptionLabel("Mahony Filter")
    .setPosition(240, 305)
    .setColorValue(nexgenAqua)
    .setColorBackground(darkGrey)
    .setBarHeight(30)
    .setWidth(400)
    .setItemHeight(30)
    .setOpen(false)
    ;

  for (int i = 0; i < IMU_DATA.length; i++) 
    imuDataList.addItem(IMU_DATA[i], i);

  cp5testIMU.addCanvas(dc); 
  cp5testIMU.hide();

  /******************************************************************
   CP5 - Calibrate ESC Screen
   ******************************************************************/

  cp5calibrateESC.addButton("mainMenu")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 180, height - 80)
    .setSize(150, 40)
    .setCaptionLabel("Main Menu")
    .setBroadcast(true)
    ;

  nextButton = cp5calibrateESC.addButton("nextStep")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 180, height - 130)
    .setSize(150, 40)
    .setCaptionLabel("Next")
    .setColorBackground(darkGreen)
    .setBroadcast(true)
    ;

  armCalibrateToggle = cp5calibrateESC.addToggle("armMotors")
    .setBroadcast(false)
    .setValue(false)
    .setPosition(width - 350, 540)
    .setSize(40, 20)
    .setMode(ControlP5.SWITCH)
    .setCaptionLabel("Arm Motors")
    .setBroadcast(true)
    ;

  masterCalibrateThrottle = cp5calibrateESC.addKnob("masterThrottleKnob")
    .setBroadcast(false)
    .setRange(0, 100)
    .setValue(0)
    .setPosition(width - 350, height - 150)
    .setRadius(50)
    .setNumberOfTickMarks(10)
    .setTickMarkLength(4)
    .setViewStyle(Knob.ARC)
    .setDragDirection(Knob.VERTICAL)
    .setCaptionLabel("Throttle")
    .setLock(true)
    .setBroadcast(true)
    ;

  mc = new DroneCanvas();
  mc.pre(); 
  mc.motorsOnly = true;
  mc.throttleOffset = 0;

  cp5calibrateESC.addCanvas(mc); 
  cp5calibrateESC.hide();

  /******************************************************************
   CP5 - Test Motors Screen
   ******************************************************************/

  cp5testMotors.addButton("mainMenu")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 180, height - 80)
    .setSize(150, 40)
    .setCaptionLabel("Main Menu")
    .setBroadcast(true)
    ;

  armTestToggle = cp5testMotors.addToggle("armMotors")
    .setBroadcast(false)
    .setValue(false)
    .setPosition(width - 350, 540)
    .setSize(40, 20)
    .setMode(ControlP5.SWITCH)
    .setCaptionLabel("Arm Motors")
    .setBroadcast(true)
    ;

  masterTestThrottle = cp5testMotors.addKnob("masterThrottleKnob")
    .setBroadcast(false)
    .setRange(0, 100)
    .setValue(0)
    .setPosition(width - 350, height - 150)
    .setRadius(50)
    .setNumberOfTickMarks(10)
    .setTickMarkLength(4)
    .setViewStyle(Knob.ARC)
    .setDragDirection(Knob.VERTICAL)
    .setCaptionLabel("Throttle")
    .setLock(true)
    .setBroadcast(true)
    ;

  m1dutyCycle = cp5testMotors.addSlider("sliderM1")
    .setBroadcast(false)
    .setRange(0, 100)
    .setValue(0)
    .setPosition(30, 490)
    .setSize(15, 100)
    .setNumberOfTickMarks(10)
    .snapToTickMarks(false)
    .setColorValue(0xffff88ff)
    .setColorLabel(0xffdddddd)
    .setCaptionLabel("        M1 (CCW) Duty Cycle: 0%")
    .setLock(true)
    .setBroadcast(true)
    ;

  m2dutyCycle = cp5testMotors.addSlider("sliderM2")
    .setBroadcast(false)
    .setRange(0, 100)
    .setValue(0)
    .setPosition(300, 490)
    .setSize(15, 100)
    .setNumberOfTickMarks(10)
    .snapToTickMarks(false)
    .setColorValue(0xffff88ff)
    .setColorLabel(0xffdddddd)
    .setCaptionLabel("        M2 (CW) Duty Cycle: 0%")
    .setLock(true)
    .setBroadcast(true)
    ;

  m3dutyCycle = cp5testMotors.addSlider("sliderM3")
    .setBroadcast(false)
    .setRange(0, 100)
    .setValue(0)
    .setPosition(300, 230)
    .setSize(15, 100)
    .setNumberOfTickMarks(10)
    .snapToTickMarks(false)
    .setColorValue(0xffff88ff)
    .setColorLabel(0xffdddddd)
    .setCaptionLabel("        M3 (CCW) Duty Cycle: 0%")
    .setLock(true)
    .setBroadcast(true)
    ;

  m4dutyCycle = cp5testMotors.addSlider("sliderM4")
    .setBroadcast(false)
    .setRange(0, 100)
    .setValue(0)
    .setPosition(30, 230)
    .setSize(15, 100)
    .setNumberOfTickMarks(10)
    .snapToTickMarks(false)
    .setColorValue(0xffff88ff)
    .setColorLabel(0xffdddddd)
    .setCaptionLabel("        M4 (CW) Duty Cycle: 0%")
    .setLock(true)
    .setBroadcast(true)
    ;

  cp5testMotors.addCanvas(mc); 
  cp5testMotors.hide();
  
  /******************************************************************
   CP5 - Tune PID Screen
   ******************************************************************/
   
  cascadedPID = loadImage("CascadedPID.png");

  cp5tunePID.addButton("mainMenu")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(width - 180, height - 80)
    .setSize(150, 40)
    .setCaptionLabel("Main Menu")
    .setBroadcast(true)
    ;
    
  cp5tunePID.addButton("tuneRoll")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(500, 123)
    .setSize(120, 20)
    .setCaptionLabel("Tune Roll")
    .setBroadcast(true)
    ;
    
  cp5tunePID.addButton("tunePitch")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(500, 147)
    .setSize(120, 20)
    .setCaptionLabel("Tune Pitch")
    .setBroadcast(true)
    ;
    
  cp5tunePID.addButton("tuneYaw")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(500, 171)
    .setSize(120, 20)
    .setCaptionLabel("Tune Yaw")
    .setBroadcast(true)
    ;
    
  cp5tunePID.addButton("requestLoopFreq")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(545, 605)
    .setSize(90, 25)
    .setCaptionLabel("Request")
    .setColorBackground(darkGrey)
    .setBroadcast(true)
    ;

  cp5tunePID.addCanvas(dc); 
  cp5tunePID.hide();

  /******************************************************************
   CP5 - SETUP DONE
   
   Note:
   
   1. Java OpenGL (JOGL) is a wrapper library that allows OpenGL to be used in Java/Processing.
      There is an exception thrown if setup() takes more than 5 seconds to finish after JOGL initialises the canvas.
      Consequently, do this at the end of setup().
   ******************************************************************/

  frameRate(60);
  size(1024, 768, P3D); 
  noSmooth();
  logConfigurator("Nexgen Configurator loaded - version: " + VERSION);
}

/******************************************************************
 Add Chart Datasets - Test IMU
 ******************************************************************/

void addRollDataset() {
  imuChart.addDataSet("roll");
  imuChart.setColors("roll", ROLL_FILTER_COLOUR, ROLL_FILTER_COLOUR);
  imuChart.setData("roll", new float[100]);
}

void addPitchDataset() {
  imuChart.addDataSet("pitch");
  imuChart.setColors("pitch", PITCH_FILTER_COLOUR, PITCH_FILTER_COLOUR);
  imuChart.setData("pitch", new float[100]);
}

void addYawDataset() {
  imuChart.addDataSet("yaw");
  imuChart.setColors("yaw", YAW_FILTER_COLOUR, YAW_FILTER_COLOUR);
  imuChart.setData("yaw", new float[100]);
}

/******************************************************************
 SBUS Conversion to Strings - Radio Control
 ******************************************************************/

String[] sbusToStringArray() {
  //  RC Input Conversions
  float rcThrottle = map(float(channels[0]), 172, 1811, 0, 150);  // Throttle = SBUS channel 0
  float rcRoll = map(float(channels[1]), 172, 1811, 0, 150);      // Roll     = SBUS channel 1
  float rcPitch = map(float(channels[2]), 172, 1811, 0, 150);     // Pitch    = SBUS channel 2
  float rcYaw = map(float(channels[3]), 172, 1811, 0, 150);       // Yaw      = SBUS channel 3

  float throttlePercentage = (rcThrottle / 150.0) * 100.0;
  float rollPercentage = ((rcRoll - 75.0) / 150.0) * 100.0;
  float pitchPercentage = ((rcPitch - 75.0) / 150.0) * 100.0;
  float yawPercentage = ((rcYaw - 75.0) / 150.0) * 100.0;

  String throttleString = int(throttlePercentage) + "%";
  String rollString = int(rollPercentage) + "°";
  String pitchString = int(pitchPercentage) + "°";
  String yawString = int(yawPercentage) + "°"; 
  
  String buzzerString = "OFF";
  String rssiString = rssi + "dB";
  String rxVoltageString = nf(rxVoltage, 0, 1) + "V";
  String armString = "NO"; 
  String flightModeString = "RATE";
  
  if (channels[4] > SWITCH_ON) { buzzerString = "ON"; }
  if (channels[7] > SWITCH_ON) { armString = "YES"; }  
  if (channels[8] > SWITCH_ON) { flightModeString = "STABILIZE"; }
    
  return new String[] {throttleString, rollString, pitchString, yawString, buzzerString, rssiString, rxVoltageString, armString, flightModeString};
}

/******************************************************************
 DRAW - Render Methods
 ******************************************************************/

void dashedLine(int x1, int y1, int lineWidth) {
  // Draws a horizontal dashed line
  for (int x = x1; x < x1 + lineWidth; x += 6) {
    stroke(-(x+y1>>1 & 1));
    line(x, y1, x+5, y1);
  }
}

void setGradient(int x, int y, float w, float h, color c1, color c2, int axis ) {
  pushMatrix();
  noFill();

  if (axis == Y_AXIS) {  // Top to bottom gradient
    for (int i = y; i <= y+h; i++) {
      float inter = map(i, y, y+h, 0, 1);
      color c = lerpColor(c1, c2, inter);
      stroke(c);
      line(x, i, x+w, i);
    }
  }  
  else if (axis == X_AXIS) {  // Left to right gradient
    for (int i = x; i <= x+w; i++) {
      float inter = map(i, x, x+w, 0, 1);
      color c = lerpColor(c1, c2, inter);
      stroke(c);
      line(i, y, i, y+h);
    }
  }
  
  popMatrix();
}

void drawArrow(int x, int y, int width) {
  pushMatrix();
  strokeWeight(2);
  stroke(nexgenAqua);
  fill(nexgenAqua);
  beginShape();
  vertex(x, y);
  vertex(x + width, y);
  vertex(x + width/2, y - width);
  endShape(CLOSE);
  rectMode(CORNER);
  rect(x + 15, y, width - 30, width * 0.8);
  popMatrix();
}

void warningIcon(int x, int y, int width) {
  pushMatrix();
  strokeWeight(2);
  stroke(cp5yellow);
  fill(0);
  triangle(x, y, x + width, y, x + width/2, y - width);
  stroke(0);
  strokeWeight(2);
  triangle(x + 4, y - 3, x + width - 4, y - 3, x + width/2, y - width + 4);
  stroke(cp5yellow);
  fill(cp5yellow);
  textFont(bold);
  text("!", x + width/2 - 4, y - 4);
  popMatrix();
}

void propWarning(int x, int y) {
  pushMatrix();
  stroke(cp5yellow);
  textFont(smallBold);
  text("WARNING - REMOVE PROPELLERS", x, y - 19);
  text("BEFORE ARMING DRONE!!", x + 81, y - 4);
  popMatrix();
}

void liPoWarning(int x, int y) {
  pushMatrix();
  stroke(cp5yellow);
  textFont(smallBold);
  text("WARNING - Motor Battery disconnected or", x, y - 19);
  text("requires recharging!", x + 81, y - 4);
  popMatrix();
}

void drawAxis(int x, int y, int chartWidth) {
  stroke(0);
  strokeWeight(2);

  //  Main Axis and Tick Marks
  line(x, y, x + chartWidth, y);
  for (int tick = 60; tick < x + chartWidth; tick = tick + 20) {
    line(tick, y + 5, tick, y - 5);
  }

  //  90 Degree Lines
  dashedLine(x, y + y/4, chartWidth);
  dashedLine(x, y - y/4, chartWidth);

  //  Y-Axis Labels
  fill(WHITE);
  stroke(WHITE);
  line(x - 5, y + y/4, x, y + y/4);
  line(x - 5, y - y/4, x, y - y/4);
  textFont(regular);
  text(" 90°", x - 35, y - y/4);
  text("-90°", x - 35, y + y/4);
}

void drawBackground()
{
  String version = "version " + VERSION;
  
  pushMatrix();
  fill(WHITE);
  textSize(24);
  textFont(bold);
  textAlign(LEFT);
  text("Nexgen Configurator", 20, 30);
  textSize(16);
  textFont(smallBold);
  text(version, 20, 50);
  setGradient(270, 10, 365, 30, color(0), nexgenGold, X_AXIS);
  if (state != AppState.MAIN_MENU) {
    fill(nexgenGold);
    text(subMenu, 520, 52); 
  }
  popMatrix();
}

void drawRSSI(int x, int y)
{
  String rssiString = str(rssi) + "dB";

  stroke(WHITE);
  strokeWeight(1);

  if (rssi <= 25) {
    fill(cp5red);
    rect(x, y-10, 5, 10);
    fill(0);
    rect(x+8, y-15, 5, 15);
    rect(x+16, y-20, 5, 20);
    rect(x+24, y-25, 5, 25);
  } else if (rssi <= 50) {
    fill(nexgenAqua);
    rect(x, y-10, 5, 10);
    rect(x+8, y-15, 5, 15);
    fill(0);
    rect(x+16, y-20, 5, 20);
    rect(x+24, y-25, 5, 25);
  } else if (rssi <= 75) {
    fill(nexgenAqua);
    rect(x, y-10, 5, 10);
    rect(x+8, y-15, 5, 15);
    rect(x+16, y-20, 5, 20);
    fill(0);
    rect(x+24, y-25, 5, 25);
  } else {
    fill(nexgenAqua);
    rect(x, y-10, 5, 10);
    rect(x+8, y-15, 5, 15);
    rect(x+16, y-20, 5, 20);
    rect(x+24, y-25, 5, 25);
  }

  fill(nexgenAqua);
  textFont(regular);
  text(rssiString, x, y+20);
}

void drawBattery(int x, int y, float voltage, float fullCharge)
{
  String batteryVoltage = nf(voltage, 0, 1) + "V";
  float percentageCharged = voltage / fullCharge;

  stroke(WHITE);
  strokeWeight(2);
  if (percentageCharged <= 0.25) {
    fill(cp5red);
    rect(x, y, 10, 20);
    fill(0);
    rect(x+13, y, 10, 20);
    rect(x+26, y, 10, 20);
    rect(x+39, y, 10, 20);
  } else if (percentageCharged <= 0.5) {
    fill(nexgenGreen);
    rect(x, y, 10, 20);
    rect(x+13, y, 10, 20);
    fill(0);
    rect(x+26, y, 10, 20);
    rect(x+39, y, 10, 20);
  } else if (percentageCharged <= 0.75) {
    fill(nexgenGreen);
    rect(x, y, 10, 20);
    rect(x+13, y, 10, 20);
    rect(x+26, y, 10, 20);
    fill(0);
    rect(x+39, y, 10, 20);
  } else {
    fill(nexgenGreen);
    rect(x, y, 10, 20);
    rect(x+13, y, 10, 20);
    rect(x+26, y, 10, 20);
    rect(x+39, y, 10, 20);
  }

  fill(WHITE);
  rect(x+52, y + 5, 5, 10);

  fill(nexgenGreen);
  textSize(16);
  textFont(regular);
  text(batteryVoltage, x+70, y + 20);
}

void drawIndicators() {
  stroke(BLACK);
  fill(darkGrey);
  rect(width - 350, 370, 320, 70);
  rect(width - 350, 370, 120, 70);
  fill(WHITE);
  textSize(14);
  textFont(smallBold);
  text("LiPo", width - 340, 390);
  text("Receiver", width - 220, 390);

  drawBattery(width - 340, 410, droneBattery, FULLY_CHARGED);
  drawBattery(width - 220, 410, rxVoltage, 5.0);
  drawRSSI(width - 80, 410);
}

void drawConfigFlag(int x, int y) {
  color iconColor = nexgenGreen;
  
  if (configFlag) { iconColor = cp5red; }
  fill(BLACK);
  stroke(iconColor);
  rect(x, y, 20, 20);
  line(x, y+5, x+20, y+5);
  fill(iconColor);
  text("..", x+2, y+3);
}

void drawLowBatFlag(int x, int y) {
  fill(BLACK);
  stroke(nexgenGreen);
  if (lowBatFlag) { stroke(cp5red); }
  rect(x, y, 20, 10);
  rect(x+20, y+2.5, 4, 5);
  line(x+5, y, x+5, y+10);
  line(x+10, y, x+10, y+10);
  line(x+15, y, x+15, y+10);
}

void drawBootFlag(int x, int y) {
  color iconColor = nexgenGreen;
  
  fill(BLACK);
  if (bootFlag) { iconColor = cp5red; }
  stroke(iconColor);
  strokeWeight(4);
  circle(x, y, 20);
  stroke(BLACK);
  rect(x+6, y-2, 8, 5);
  stroke(iconColor);
  line(x+10, y-4, x+10, y+8);
}

void drawAngleFlag(int x, int y) {
  pushMatrix();
  fill(BLACK);
  stroke(nexgenGreen);
  if (angleFlag) { stroke(cp5red); }
  line(x, y, x+25, y);
  line(x, y, x+20, y-20);
  strokeWeight(1);
  arc(x-2, y-11, 20, 25, PI+HALF_PI, TWO_PI);
  popMatrix();
}

void drawThrottleFlag(int x, int y) {
  color iconColor = nexgenGreen;
  
  fill(BLACK);
  if (throttleFlag) { iconColor = cp5red; }
  stroke(iconColor);
  strokeWeight(8);
  arc(x, y, 25, 25, PI, TWO_PI);
  strokeWeight(3);
  stroke(BLACK);
  line(x+12, y-5, x+12, y+15);
  line(x-4, y-5, x+12, y+15);
  line(x+28, y-5, x+12, y+15);
  strokeWeight(2);
  stroke(iconColor);
  circle(x+12, y+15, 3);
  stroke(nexgenGold);
  line(x+28, y+5, x+13, y+17);
}

void drawRXLossFlag(int x, int y) {
  stroke(nexgenGreen);
  if (rxLossFlag) { stroke(cp5red); }
  line(x, y, x, y+20);
  line(x-10, y, x+10, y);
  line(x-10, y, x, y+10);
  line(x+10, y, x, y+10);
}

void drawFailSafeFlag(int x, int y) {
  color iconColor = nexgenGreen;
  
  if (failSafeFlag) { iconColor = cp5red; }
  stroke(iconColor);
  fill(iconColor);
  line(x, y, x+20, y);
  line(x-2, y+2, x-2, y+20);
  strokeWeight(1);
  line(x+22, y+2, x+22, y+20);
  line(x, y+22, x+20, y+22);
  textSize(12);
  fill(nexgenGold);
  text("FS", x+2, y+16);
}

void drawNoGyroFlag(int x, int y) {
  color iconColor = nexgenGreen;
  
  if (noGyroFlag) { iconColor = cp5red; }
  stroke(iconColor);
  fill(BLACK);
  strokeWeight(1);
  circle(x, y, 20);
  fill(iconColor);
  triangle(x+6, y+9, x+14, y+9, x+10, y-4);
  triangle(x+6, y+9, x+14, y+9, x+10, y+24);
  triangle(x+10, y+5, x+10, y+15, x-3, y+9);
  triangle(x+10, y+5, x+10, y+15, x+23, y+9);
  stroke(BLACK);
  fill(BLACK);
  strokeWeight(2);
  circle(x+8, y+8, 4);
}

void drawArmingFlags(int x, int y) {
  stroke(WHITE);
  fill(BLACK);
  rect(x, y, 320, 20);
  rect(x, y+20, 320, 40);
  rect(x, y+60, 320, 20);
  fill(WHITE);
  for (int i = 0; i < 8; i++) {
    line(x+40*i, y+20, x+40*i, y+80);
    text(str(7-i), x+15+40*i, y+75);
  }
  
  fill(nexgenAqua);
  textFont(bold);
  textSize(14); 
  text("ARM Prevention Flags", x+85, y+15);
  
  //  Arm Prevention Flag Icons
  drawConfigFlag(x+10, y+30);
  drawLowBatFlag(x+47, y+35);
  drawBootFlag(x+90, y+31);
  drawAngleFlag(x+128, y+48);
  drawThrottleFlag(x+167, y+33);
  drawRXLossFlag(x+220, y+30);
  drawFailSafeFlag(x+250, y+30);
  drawNoGyroFlag(x+290, y+31);
}

void drawLoopFrequency(int x, int y) {
  stroke(WHITE);
  fill(BLACK);
  rect(x, y, 95, 20);
  rect(x, y+20, 95, 20);
  
  fill(nexgenAqua);
  textFont(bold);
  textSize(14); 
  text("Loop Freq", x+12, y+15);
  fill(WHITE);
  text(loopFrequency + " Hz", x+37, y+35);
}

void drawSampleRate(int x, int y) {
  stroke(WHITE);
  fill(BLACK);
  rect(x, y, 95, 20);
  rect(x, y+20, 95, 20);
  rect(x, y+40, 95, 20);
  rect(x, y+60, 95, 20);
  rect(x-50, y+20, 50, 20);
  rect(x-50, y+40, 50, 20);
  rect(x-50, y+60, 50, 20);
  
  fill(nexgenAqua);
  textFont(bold);
  textSize(14); 
  text("Sample Rate", x+5, y+15);
  
  fill(WHITE);
  if (selectedIMU == IMUType.LSM9DS1) {
    text(nf(gyroSampleRate, 0, 1) + " Hz", x+20, y+35);
    text(nf(accelSampleRate, 0, 1) + " Hz", x+20, y+55);
    text(nf(magSampleRate, 0, 1) + " Hz", x+20, y+75);
  }
  else {
    text("2.6 kHz", x+25, y+35);
    text("1 kHz", x+30, y+55);
    text("N/A", x+35, y+75);
  }
  
  fill(cp5magenta);
  text("Gyro", x-40, y+35);
  
  fill(cp5orange);
  text("Acc", x-40, y+55);
  
  fill(nexgenGold);
  text("Mag", x-40, y+75);
}

void drawFrameRate(int x, int y) {
  stroke(WHITE);
  fill(BLACK);
  rect(x, y, 95, 20);
  rect(x, y+20, 95, 20);
  
  fill(nexgenAqua);
  textFont(bold);
  textSize(14); 
  text("Frame Rate", x+8, y+15);
  fill(WHITE);
  text(int(frameRate) + " fps", x+28, y+35);
}

void updateMotorDutyCycle() {
  String slider1 = "        M1 (CCW) Duty Cycle: " + mc.throttle1 + "%";
  String slider2 = "        M2 (CW) Duty Cycle: " + mc.throttle2 + "%";
  String slider3 = "        M3 (CCW) Duty Cycle: " + mc.throttle3 + "%";
  String slider4 = "        M4 (CW) Duty Cycle: " + mc.throttle4 + "%";

  m1dutyCycle.setCaptionLabel(slider1);
  m1dutyCycle.setValue(mc.throttle1);
  m2dutyCycle.setCaptionLabel(slider2);
  m2dutyCycle.setValue(mc.throttle2);
  m3dutyCycle.setCaptionLabel(slider3);
  m3dutyCycle.setValue(mc.throttle3);
  m4dutyCycle.setCaptionLabel(slider4);
  m4dutyCycle.setValue(mc.throttle4);
}

void displayInstruction(int x, int y, String instruction) {
  fill(WHITE);
  textFont(bold);
  textSize(18); 
  text(instruction, x, y);
}

void drawRCBackground() {
  stroke(BLACK);
  fill(darkGrey);
  rect(20, 160, 620, 190);   // SBUS Channels Background Rect  
  
  //  Title, armed and lost frames message
  stroke(WHITE);
  fill(BLACK);
  rect(143, 105, 95, 20);
  rect(143, 125, 95, 20);
  rect(243, 105, 95, 20);
  rect(243, 125, 95, 20);
  rect(343, 105, 95, 20);
  rect(343, 125, 95, 20);
  rect(443, 105, 95, 20);
  rect(443, 125, 95, 20);
  rect(543, 105, 95, 20);
  rect(543, 125, 95, 20);
  
  fill(nexgenAqua);
  textFont(bold);
  textSize(24);
  text("SBUS", 20, 130);
  textSize(14); 
  text("Fail Safe", 160, 120);
  text("Connected", 252, 120);
  text("Flight Mode", 349, 120);
  text("Lost Frames", 450, 120);
  text(channels[10], 480, 140);
  text("Armed", 565, 120);
  
  if (channels[9] > SWITCH_ON) {
    fill(cp5red);
    text("YES", 178, 140);
  }
  else {
    text("NO", 180, 140);
  }
  
  if (sbusAvailable) {
    text("YES", 275, 140);
  }
  else {
    fill(cp5red);
    text("NO", 278, 140);
  }
  
  if (channels[7] > SWITCH_ON) {
    fill(cp5red);
    text("YES", 575, 140);
  }
  else {
    fill(WHITE);
    text("NO", 578, 140);
  }
  
  fill(WHITE);
  if (channels[8] > SWITCH_ON) {
    text("STABILIZE", 354, 140);
  }
  else {
    text("RATE", 370, 140);
  }
}

void indicatorsForDroneState() {
/******************************************************************
 
  LED State Indication  - DISCONNECTED    Blinking Blue, Solid Yellow
                        - USB_SERIAL      Solid Magenta, Breathing Yellow
                        - SBUS            Blinking Green, Solid Yellow
                        - USB_SBUS        Solid Cyan, Breathing Yellow
                        - FATAL_ERROR     Blinking Red, Blinking Yellow
                        - ARMED           Solid Green, Yellow Off
                        
 ******************************************************************/
  int elapsedTime = millis() - time;
  color rgbLED = cp5black, yellowLED = cp5black;
  boolean rgbLEDblink = false, yellowLEDblink = false;
  
  switch(droneState) {
    case DISCONNECTED:
      rgbLED = lightBlue;
      rgbLEDblink = true;
      yellowLED = cp5yellow;
      yellowLEDblink = false;
      break;
    case USB_SERIAL:
      rgbLED = cp5magenta;
      rgbLEDblink = false;
      yellowLED = cp5yellow;
      yellowLEDblink = true;
      break;
    case SBUS:
      rgbLED = nexgenGreen;
      rgbLEDblink = true;
      yellowLED = cp5yellow;
      yellowLEDblink = false;
      break;
    case USB_SBUS:
      rgbLED = cp5cyan;
      rgbLEDblink = false;
      yellowLED = cp5yellow;
      yellowLEDblink = true;
      break;
    case ARMED:
      rgbLED = nexgenGreen;
      rgbLEDblink = false;
      yellowLED = cp5black;
      yellowLEDblink = false;
      break;
    case FATAL_ERROR:
      rgbLED = cp5red;
      rgbLEDblink = true;
      yellowLED = cp5yellow;
      yellowLEDblink = true;
      break;
    default:
      break; 
    }
    
  strokeWeight(2);
  
  //  Power LED's
  /*
   *  fill(cp5red);
   *  circle(390, 250, 8);
   *  fill(cp5green);
   *  circle(452, 250, 8);
   */
  
  fill(rgbLED);
  if (rgbLEDblink && elapsedTime >= 500) {
    fill(darkGrey);
  }
  circle(570, 510, 20);  //  LED on dashboard
  circle(420, 265, 10);  //  LED on Drone Model
  
  fill(yellowLED);
  if (yellowLEDblink && elapsedTime < 500) {
    fill(darkGrey);
  }
  circle(600, 510, 20);  //  LED on dashboard
  circle(420, 295, 10);  //  LED on Drone Model
  
  if (elapsedTime >= 1000) {
    time = millis();
  }
}

void drawIMUscale(int x, int y) {
  pushMatrix();
  stroke(WHITE);
  strokeWeight(2);
  line(x, y, x, y-10);
  line(x, y, x+220, y);
  line(x+220, y, x+220, y-10);
  line(x+110, y, x+110, y-10);
  popMatrix();
}

void drawIMUbias(int x, int y) {
  stroke(WHITE);
  strokeWeight(2);
  fill(BLACK);
  rect(x, y, 350, 80);
  line(x, y+20, x+350, y+20);
  line(x+80, y, x+80, y+80);
  line(x+170, y, x+170, y+80);
  line(x+260, y, x+260, y+80);
  
  fill(nexgenAqua);
  textFont(bold);
  textSize(14); 
  text("IMU BIAS", x+8, y+15);
  
  fill(cp5magenta);
  text("Gyro", x+5, y+35);
  
  fill(cp5orange);
  text("Acc", x+5, y+55);
  
  fill(nexgenGold);
  text("Mag", x+5, y+75);
  
  fill(cp5green);
  text("X", x+120, y+15);
  
  fill(cp5cyan);
  text("Y", x+210, y+15);
  
  fill(cp5red);
  text("Z", x+300, y+15);
  
  fill(WHITE);
  text(gx_bias, x+95, y+35); text(gy_bias, x+185, y+35); text(gz_bias, x+275, y+35);
  text(ax_bias, x+95, y+55); text(ay_bias, x+185, y+55); text(az_bias, x+275, y+55);
  text(mx_bias, x+95, y+75); text(my_bias, x+185, y+75); text(mz_bias, x+275, y+75);
}

void draw() 
{
  background(BLACK);
  lights();

  drawBackground(); 
  drawIndicators();
  
  textFont(bold);
  textSize(12);
  fill(WHITE);
  text(day()+"/"+month()+"/"+year()+" - "+hour()+":"+minute()+":"+second(), 520, 740);

  switch (state) {
  case MAIN_MENU:
    String commState = "Drone State: " + DRONE_STATES[droneState.getValue()];
  
    subMenu = "";
    //  Magpie Drone Schematic
    fill(WHITE);
    image(magpieDrone, 250, 110);
    
    stroke(BLACK);
    fill(darkGrey);
    rect(40, 500, 600, 40);   // Drone Dashboard
    
    fill(WHITE);
    textFont(bold);
    textSize(14);
    text(commState, 60, 525);
    
    indicatorsForDroneState();
    drawLoopFrequency(543, 555);
    drawArmingFlags(40, 555);
    drawFrameRate(543, 640);
    break;
  case CALIBRATE_IMU:
    subMenu = "[  Calibrate IMU  ]";
    stroke(BLACK);
    fill(darkGrey);
    rect(20, 100, 620, 110);   // Gyroscope Background Rect   
    rect(20, 78, 125, 20);     // Gyroscope Tab Rect
    
    rect(20, 247, 620, 110);   // Accelerometer Background Rect
    rect(20, 225, 125, 20);    // Accelerometer Tab Rect
    
    rect(20, 394, 620, 110);   // Magnetometer Background Rect
    rect(20, 372, 125, 20);    // Magnetometer Tab Rect
    
    textSize(14);
    textFont(smallBold);
    
    //  GYRO
    fill(cp5magenta);
    text("Gyroscope", 30, 93);
    text("DPS", 60, 120);
    text("Max", 480, 120);
    text("Avg", 420, 120);
    text("Min", 360, 120);
    text("0", 232, 120);
    
    fill(WHITE);
    text("X:", 30, 140);
    text("Y:", 30, 160);
    text("Z:", 30, 180);
    
    text(nf(gyr_x, 0, 1), 65, 140);
    text(nf(gyr_y, 0, 1), 65, 160);
    text(nf(gyr_z, 0, 1), 65, 180);
    
    text(nf(gyr_min_x, 0, 1), 360, 140);
    text(nf(gyr_min_y, 0, 1), 360, 160);
    text(nf(gyr_min_z, 0, 1), 360, 180);
    
    text(nf(gyr_avg_x, 0, 1), 420, 140);
    text(nf(gyr_avg_y, 0, 1), 420, 160);
    text(nf(gyr_avg_z, 0, 1), 420, 180);
    
    text(nf(gyr_max_x, 0, 1), 480, 140);
    text(nf(gyr_max_y, 0, 1), 480, 160);
    text(nf(gyr_max_z, 0, 1), 480, 180);
    
    drawIMUscale(125, 140);  //  Gyro - X
    drawIMUscale(125, 160);  //  Gyro - Y
    drawIMUscale(125, 180);  //  Gyro - Z
    
    //  Gyro Bar Charts
    final float BAR_WIDTH = 108.0;
    final float ZERO_POSITION = 235.0;
    
    float gyroXPercentage = (gyr_x / GYRO_RANGE) * BAR_WIDTH;
    float gyroMaxXPercentage = (gyr_max_x / GYRO_RANGE) * BAR_WIDTH;
    float gyroMinXPercentage = (gyr_min_x / GYRO_RANGE) * BAR_WIDTH;
    
    fill(cp5green);
    stroke(cp5green);
    if (gyr_x < 0) {
      rect(ZERO_POSITION + gyroXPercentage, 130, abs(gyroXPercentage), 8);
    }
    else if (gyr_x > 0) {
      rect(ZERO_POSITION, 130, gyroXPercentage, 8);
    }
    else {
      strokeWeight(2);
      line(ZERO_POSITION, 140, ZERO_POSITION, 130);
    }
    
    //  Gyro X Max/Min markers
    strokeWeight(2);
    stroke(cp5red);
    line(ZERO_POSITION + gyroMinXPercentage, 140, ZERO_POSITION + gyroMinXPercentage, 130);
    line(ZERO_POSITION + gyroMaxXPercentage, 140, ZERO_POSITION + gyroMaxXPercentage, 130);
    
    float gyroYPercentage = (gyr_y / GYRO_RANGE) * BAR_WIDTH;
    float gyroMaxYPercentage = (gyr_max_y / GYRO_RANGE) * BAR_WIDTH;
    float gyroMinYPercentage = (gyr_min_y / GYRO_RANGE) * BAR_WIDTH;
    
    fill(cp5cyan);
    stroke(cp5cyan);
    if (gyr_y < 0) {
      rect(ZERO_POSITION + gyroYPercentage, 150, abs(gyroYPercentage), 8);
    }
    else if (gyr_y > 0) {
      rect(ZERO_POSITION, 150, gyroYPercentage, 8);
    }
    else {
      strokeWeight(2);
      line(ZERO_POSITION, 160, ZERO_POSITION, 150);
    }
    
    //  Gyro Y Max/Min markers
    strokeWeight(2);
    stroke(cp5red);
    line(ZERO_POSITION + gyroMinYPercentage, 150, ZERO_POSITION + gyroMinYPercentage, 160);
    line(ZERO_POSITION + gyroMaxYPercentage, 150, ZERO_POSITION + gyroMaxYPercentage, 160);
    
    float gyroZPercentage = (gyr_z / GYRO_RANGE) * BAR_WIDTH;
    float gyroMaxZPercentage = (gyr_max_z / GYRO_RANGE) * BAR_WIDTH;
    float gyroMinZPercentage = (gyr_min_z / GYRO_RANGE) * BAR_WIDTH;
    
    fill(cp5red);
    stroke(cp5red);
    if (gyr_z < 0) {
      rect(ZERO_POSITION + gyroZPercentage, 170, abs(gyroZPercentage), 8);
    }
    else if (gyr_z > 0) {
      rect(ZERO_POSITION, 170, gyroZPercentage, 8);
    }
    else {
      strokeWeight(2);
      line(ZERO_POSITION, 180, ZERO_POSITION, 170);
    }
    
    //  Gyro Z Max/Min markers
    strokeWeight(2);
    stroke(cp5red);
    line(ZERO_POSITION + gyroMinZPercentage, 180, ZERO_POSITION + gyroMinZPercentage, 170);
    line(ZERO_POSITION + gyroMaxZPercentage, 180, ZERO_POSITION + gyroMaxZPercentage, 170);
    
    //  ACCELEROMETER
    fill(cp5orange);
    text("Accelerometer", 30, 240);
    text("G", 70, 267);
    text("Max", 480, 267);
    text("Avg", 420, 267);
    text("Min", 360, 267);
    text("0", 232, 267);
    
    fill(WHITE);
    text("X:", 30, 287);
    text("Y:", 30, 307);
    text("Z:", 30, 327);
    
    text(nf(acc_x, 0, 1), 65, 287);
    text(nf(acc_y, 0, 1), 65, 307);
    text(nf(acc_z, 0, 1), 65, 327);
    
    text(nf(acc_min_x, 0, 1), 360, 287);
    text(nf(acc_min_y, 0, 1), 360, 307);
    text(nf(acc_min_z, 0, 1), 360, 327);
    
    text(nf(acc_avg_x, 0, 1), 420, 287);
    text(nf(acc_avg_y, 0, 1), 420, 307);
    text(nf(acc_avg_z, 0, 1), 420, 327);
    
    text(nf(acc_max_x, 0, 1), 480, 287);
    text(nf(acc_max_y, 0, 1), 480, 307);
    text(nf(acc_max_z, 0, 1), 480, 327);
    
    drawIMUscale(125, 287);  //  Accel - X
    drawIMUscale(125, 307);  //  Accel - Y
    drawIMUscale(125, 327);  //  Accel - Z
    
    //  Acc X Bar Charts
    float accelXPercentage = (acc_x / ACCEL_RANGE) * BAR_WIDTH;
    float accelMaxXPercentage = (acc_max_x / ACCEL_RANGE) * BAR_WIDTH;
    float accelMinXPercentage = (acc_min_x / ACCEL_RANGE) * BAR_WIDTH;
    
    fill(cp5green);
    stroke(cp5green);
    if (acc_x < 0) {
      rect(ZERO_POSITION + accelXPercentage, 277, abs(accelXPercentage), 8);
    }
    else if (acc_x > 0) {
      rect(ZERO_POSITION, 277, accelXPercentage, 8);
    }
    else {
      strokeWeight(2);
      line(ZERO_POSITION, 287, ZERO_POSITION, 277);
    }
    
    //  Accel X Max/Min markers
    strokeWeight(2);
    stroke(cp5red);
    line(ZERO_POSITION + accelMinXPercentage, 287, ZERO_POSITION + accelMinXPercentage, 277);
    line(ZERO_POSITION + accelMaxXPercentage, 287, ZERO_POSITION + accelMaxXPercentage, 277);
    
    float accelYPercentage = (acc_y / ACCEL_RANGE) * BAR_WIDTH;
    float accelMaxYPercentage = (acc_max_y / ACCEL_RANGE) * BAR_WIDTH;
    float accelMinYPercentage = (acc_min_y / ACCEL_RANGE) * BAR_WIDTH;
    
    fill(cp5cyan);
    stroke(cp5cyan);
    if (acc_y < 0) {
      rect(ZERO_POSITION + accelYPercentage, 297, abs(accelYPercentage), 8);
    }
    else if (acc_y > 0) {
      rect(ZERO_POSITION, 297, accelYPercentage, 8);
    }
    else {
      strokeWeight(2);
      line(235, 307, 235, 297);
    }
    
    //  Accel Y Max/Min markers
    strokeWeight(2);
    stroke(cp5red);
    line(ZERO_POSITION + accelMinYPercentage, 297, ZERO_POSITION + accelMinYPercentage, 307);
    line(ZERO_POSITION + accelMaxYPercentage, 297, ZERO_POSITION + accelMaxYPercentage, 307);
    
    float accelZPercentage = (acc_z / ACCEL_RANGE) * BAR_WIDTH;
    float accelMaxZPercentage = (acc_max_z / ACCEL_RANGE) * BAR_WIDTH;
    float accelMinZPercentage = (acc_min_z / ACCEL_RANGE) * BAR_WIDTH;
    
    fill(cp5red);
    stroke(cp5red);
    if (acc_z < 0) {
      rect(ZERO_POSITION + accelZPercentage, 317, abs(accelZPercentage), 8);
    }
    else if (acc_z > 0) {
      rect(ZERO_POSITION, 317, accelZPercentage, 8);
    }
    else {
      strokeWeight(2);
      line(ZERO_POSITION, 327, ZERO_POSITION, 317);
    }
    
    //  Accel Z Max/Min markers
    strokeWeight(2);
    stroke(cp5red);
    line(ZERO_POSITION + accelMinZPercentage, 317, ZERO_POSITION + accelMinZPercentage, 327);
    line(ZERO_POSITION + accelMaxZPercentage, 317, ZERO_POSITION + accelMaxZPercentage, 327);
    
    //  MAGNETOMETER
    fill(nexgenGold);
    text("Magnetometer", 30, 387);
    text("µT", 68, 414);
    text("Max", 480, 414);
    text("Avg", 420, 414);
    text("Min", 360, 414);
    text("0", 232, 414);
    
    fill(WHITE);
    text("X:", 30, 434);
    text("Y:", 30, 454);
    text("Z:", 30, 474);
    
    text(nf(mag_x, 0, 1), 65, 434);
    text(nf(mag_y, 0, 1), 65, 454);
    text(nf(mag_z, 0, 1), 65, 474);
    
    text(nf(mag_min_x, 0, 1), 360, 434);
    text(nf(mag_min_y, 0, 1), 360, 454);
    text(nf(mag_min_z, 0, 1), 360, 474);
    
    text(nf(mag_avg_x, 0, 1), 420, 434);
    text(nf(mag_avg_y, 0, 1), 420, 454);
    text(nf(mag_avg_z, 0, 1), 420, 474);
    
    text(nf(mag_max_x, 0, 1), 480, 434);
    text(nf(mag_max_y, 0, 1), 480, 454);
    text(nf(mag_max_z, 0, 1), 480, 474);
    
    drawIMUscale(125, 434);  //  Mag - X
    drawIMUscale(125, 454);  //  Mag - Y
    drawIMUscale(125, 474);  //  Mag - Z
    
    //  Mag Bar Charts
    float magXPercentage = (mag_x / MAG_RANGE) * BAR_WIDTH;
    float magMaxXPercentage = (mag_max_x / MAG_RANGE) * BAR_WIDTH;
    float magMinXPercentage = (mag_min_x / MAG_RANGE) * BAR_WIDTH;
    
    fill(cp5green);
    stroke(cp5green);
    if (mag_x < 0) {
      rect(ZERO_POSITION + magXPercentage, 424, abs(magXPercentage), 8);
    }
    else if (mag_x > 0) {
      rect(ZERO_POSITION, 424, magXPercentage, 8);
    }
    else {
      strokeWeight(2);
      line(ZERO_POSITION, 434, ZERO_POSITION, 424);
    }
    
    //  Mag X Max/Min markers
    strokeWeight(2);
    stroke(cp5red);
    line(ZERO_POSITION + magMinXPercentage, 434, ZERO_POSITION + magMinXPercentage, 424);
    line(ZERO_POSITION + magMaxXPercentage, 434, ZERO_POSITION + magMaxXPercentage, 424);
    
    float magYPercentage = (mag_y / MAG_RANGE) * BAR_WIDTH;
    float magMaxYPercentage = (mag_max_y / MAG_RANGE) * BAR_WIDTH;
    float magMinYPercentage = (mag_min_y / MAG_RANGE) * BAR_WIDTH;
    
    fill(cp5cyan);
    stroke(cp5cyan);
    if (mag_y < 0) {
      rect(ZERO_POSITION + magYPercentage, 444, abs(magYPercentage), 8);
    }
    else if (mag_y > 0) {
      rect(ZERO_POSITION, 444, magYPercentage, 8);
    }
    else {
      strokeWeight(2);
      line(ZERO_POSITION, 454, ZERO_POSITION, 444);
    }
    
    //  Mag Y Max/Min markers
    strokeWeight(2);
    stroke(cp5red);
    line(ZERO_POSITION + magMinYPercentage, 454, ZERO_POSITION + magMinYPercentage, 444);
    line(ZERO_POSITION + magMaxYPercentage, 454, ZERO_POSITION + magMaxYPercentage, 444);
    
    float magZPercentage = (mag_z / MAG_RANGE) * BAR_WIDTH;
    float magMaxZPercentage = (mag_max_z / MAG_RANGE) * BAR_WIDTH;
    float magMinZPercentage = (mag_min_z / MAG_RANGE) * BAR_WIDTH;
    
    fill(cp5red);
    stroke(cp5red);
    if (mag_z < 0) {
      rect(ZERO_POSITION + magZPercentage, 464, abs(magZPercentage), 8);
    }
    else if (mag_z > 0) {
      rect(ZERO_POSITION, 464, magZPercentage, 8);
    }
    else {
      strokeWeight(2);
      line(ZERO_POSITION, 474, ZERO_POSITION, 464);
    }
    
    //  Mag Z Max/Min markers
    strokeWeight(2);
    stroke(cp5red);
    line(ZERO_POSITION + magMinZPercentage, 474, ZERO_POSITION + magMinZPercentage, 464);
    line(ZERO_POSITION + magMaxZPercentage, 474, ZERO_POSITION + magMaxZPercentage, 464);
    
    drawIMUbias(20, 555);
    drawSampleRate(438, 555);
    drawLoopFrequency(543, 555);
    drawFrameRate(543, 640);
    break;
  case TEST_IMU:
    float[] chartPos = imuChart.getPosition();
    int chartWidth = imuChart.getWidth();
    int chartHeight = imuChart.getHeight();

    pushMatrix();
    subMenu = "[      Test IMU      ]";
    
    noStroke();
    fill(darkGrey);
    rect(40, 455, 600, 225);   //  Chart Controls
    
    stroke(WHITE);
    strokeWeight(2);
    rect(45, 510, 190, 40);
    rect(45, 550, 190, 40);
    rect(45, 590, 190, 40);
    rect(45, 630, 190, 40);
    rect(260, 510, 110, 40);
    rect(260, 550, 110, 40);
    rect(260, 590, 110, 40);
    
    rect(395, 510, 110, 40);
    rect(395, 550, 110, 40);
    rect(395, 590, 110, 40);
    
    rect(125, 470, 110, 200);
    rect(260, 470, 110, 200);
    rect(395, 470, 110, 200);
    
    textFont(bold);
    textSize(14);
    fill(cp5magenta);
    text("DPS", 50, 540);
    fill(cp5orange);
    text("G-FORCE", 50, 580);
    fill(nexgenGold);
    text("M-FIELD", 50, 620);
    fill(nexgenGreen);
    text("FUSED", 50, 660);
    
    //  Chart Keys
    
    noStroke();
    fill(ROLL_FILTER_COLOUR);
    rect(220, 632, 15, 37);
    fill(PITCH_FILTER_COLOUR);
    rect(355, 632, 15, 37);
    fill(YAW_FILTER_COLOUR);
    rect(490, 632, 15, 37);
    
    fill(cp5magenta);
    textSize(16);
    text(gyr_x, 145, 540);          //  Gyro angle rates (DPS)
    text(gyr_y, 280, 540);
    text(gyr_z, 415, 540);
    
    fill(cp5orange);
    text(acc_x, 145, 580);          //  Accelerometer (G force)
    text(acc_y, 280, 580);
    text(acc_z, 415, 580);
    
    fill(nexgenGold);
    text(mag_x, 145, 620);          //  Magnetic Vectors (uT)
    text(mag_y, 280, 620);
    text(mag_z, 415, 620);
    
    fill(nexgenGreen);
    text(roll, 145, 660);           //  Selected Filtered & Fused Final Values
    text(pitch, 280, 660);
    text(yaw, 415, 660);
    
    //  Chart Scale Border
    fill(cp5blue);
    stroke(WHITE);
    strokeWeight(2);
    rect(535, 565, 95, 70);    
    
    textSize(14);
    fill(nexgenGold);
    text("Chart Scale", 543, 585);
    
    drawFrameRate(535, 465);
    drawLoopFrequency(535, 515);
    
    //  Roll Selected
    if (imuSelect.getState(0)) { 
      if (imuChart.getDataSet("roll") == null) { 
        addRollDataset();
      }
      imuChart.unshift("roll", roll);
    } else {
      imuChart.removeDataSet("roll");
      imuChart.removeDataSet("raw roll");
    }

    //  Pitch Selected
    if (imuSelect.getState(1)) { 
      if (imuChart.getDataSet("pitch") == null) { 
        addPitchDataset();
      }
      imuChart.unshift("pitch", pitch);
    } else {
      imuChart.removeDataSet("pitch");
      imuChart.removeDataSet("raw pitch");
    }

    //  Yaw Selected
    if (imuSelect.getState(2)) { 
      if (imuChart.getDataSet("yaw") == null) { 
        addYawDataset();
      }
      imuChart.unshift("yaw", yaw);
    } else {
      imuChart.removeDataSet("yaw");
      imuChart.removeDataSet("raw yaw");
    }

    drawAxis(int(chartPos[0]), int(chartPos[1] + chartHeight / 2), chartWidth);
    popMatrix();

    //  Simulate Drone
    dc.roll = radians(roll);
    dc.pitch = radians(pitch);
    dc.yaw = radians(yaw);
    break;
  case RADIO_CONTROL:
    String[] sbusValues = sbusToStringArray();
    int yIncrement = 0, channelNumber = 0;
    
    subMenu = "[ Radio Control  ]";
    drawRCBackground(); 
    drawLoopFrequency(543, 555);
    drawFrameRate(543, 640);
    drawArmingFlags(40, 555);

    //Cycle through all SBUS channels and draw a bar chart for each channel value
    for (int c = 0; c < 9; c++) {
      noStroke(); //remove the graphic boarder lines
      fill(nexgenAqua); //set the colour of bar chart

      //Map Channel bar graph
      float channelAsFloat = float(channels[c]); //convert to a float
      float barWidth = map(channelAsFloat, 0, 1640, 0, 300);

      yIncrement = yIncrement + 20; //y postion of bar on the canvas
      channelNumber = channelNumber + 1;
      rect(140, yIncrement + 150, barWidth, 15);
      fill(WHITE);
      textFont(bold);
      textSize(12); 
      text("Channel " + channelNumber + " = ", 25, yIncrement + 160);
      text(channels[c], 100, yIncrement + 160);
      
      if (c < sbusValues.length) {
        text(sbusValues[c], 490, yIncrement + 160);
      }
      
      fill(nexgenGold);
      text(channelNames[c], 560, yIncrement + 160);

      if (sbusAvailable) {
        //  Simulate Drone Movement
        switch (c) {
        case 0:
          dc.throttle = map(channelAsFloat, 172, 1811, 0, 80);
          break;
        case 1:
          dc.roll = map(channelAsFloat, 172, 1811, -1.0, 1.0);
          break;
        case 2:
          dc.pitch = map(channelAsFloat, 172, 1811, -1.0, 1.0);
          break;
        case 3:
          dc.yaw = map(channelAsFloat, 172, 1811, -2.0 * PI, 2.0 * PI);
          break;
        }
      }
    } 

    switch(controlState) {
    case INC_PITCH:
      dc.incrementPitch(0.01);
      break;
    case DEC_PITCH:
      dc.incrementPitch(-0.01);
      break;
    case INC_ROLL:
      dc.incrementRoll(-0.01);
      break;
    case DEC_ROLL:
      dc.incrementRoll(0.01);
      break; 
    case INC_THROTTLE:
      dc.incrementThrottle(1);
      break;
    case DEC_THROTTLE:
      dc.incrementThrottle(-1);
      break;
    case INC_YAW:
      dc.incrementYaw(0.01);
      break;
    case DEC_YAW:
      dc.incrementYaw(-0.01);
      break;
    case RESET:
      dc.roll = 0;
      dc.pitch = 0;
      dc.yaw = 0;
      dc.throttle = 0;
      break;
    case NOP:
      break;
    default:
      break;
    }

    break;
  case CALIBRATE_ESC:
    subMenu = "[  Calibrate ESC  ]";
    warningIcon(width - 350, 500, 30);
    propWarning(width - 310, 500);
    drawArrow(270, 140, 50);

    pushMatrix();
    fill(nexgenAqua);
    textFont(bold);
    textSize(24); 
    text("ESC Calibration Instructions", 40, height - 160);
    popMatrix();

    switch(calibrationState) {
    case START:
      displayInstruction(40, height - 130, "1. Remove Propellers and disconnect LiPo Battery then click Next.");
      break;
    case CONNECT:
      if (serialPort != null) {
        displayInstruction(40, height - 130, "2. Serial port detected. Click Next.");
      } else {
        displayInstruction(40, height - 130, "2. Connect to drone via the serial port (USB) then click Next.");
      }
      break;
    case ARM:
      displayInstruction(40, height - 130, "3. Arm the drone motors by toggling the ARM switch then click Next.");
      displayInstruction(60, height - 110, "Don't adjust the throttle until instructed. This is set automatically.");
      break;
    case MAX_THROTTLE:
      displayInstruction(40, height - 130, "4. Connect the LiPo, when the ESC tones stop, click Next.");
      displayInstruction(60, height - 110, "The ESC is storing the maximum throttle position.");
      break;
    case MIN_THROTTLE:
      displayInstruction(40, height - 130, "5. Wait until the ESC tones stop again, then click Next.");
      displayInstruction(60, height - 110, "The ESC is storing the minimum throttle position.");
      break;
    case DONE:
      displayInstruction(40, height - 130, "The ESC is calibrated and the max and min throttle values stored.");
      displayInstruction(40, height - 110, "You can use the throttle or keyboard (w = inc, s = dec) to test motors.");

      //  Keyboard control of throttle
      switch(controlState) {
      case INC_THROTTLE:
        mc.incrementThrottle(1);
        masterCalibrateThrottle.setValue(mc.maxThrottle());
        break;
      case DEC_THROTTLE:
        mc.incrementThrottle(-1);
        masterCalibrateThrottle.setValue(mc.maxThrottle());
        break;
      }
      break;
    }

    break;
  case TEST_MOTORS:
    subMenu = "[   Test Motors   ]";
    warningIcon(width - 350, 500, 30);
    if (droneBattery < 5.0 && mc.armed) {
      liPoWarning(width - 310, 500);
    } else {
      propWarning(width - 310, 500);
    }
    drawArrow(270, 140, 50);

    //  Keyboard control of throttle
    switch(controlState) {
    case INC_THROTTLE:
      mc.incrementThrottle(1);
      masterTestThrottle.setValue(mc.maxThrottle());
      break;
    case DEC_THROTTLE:
      mc.incrementThrottle(-1);
      masterTestThrottle.setValue(mc.maxThrottle());
      break;
    }
    break;
  case TUNE_PID:
    pushMatrix();
    subMenu = "[     Tune PID      ]";
    
    //  Cascaded PID Flow Diagram
    image(cascadedPID, 25, 360);
    
    stroke(BLACK);
    fill(darkGrey);
    rect(20, 100, 620, 110);   // PID Parameters Background Rect   
    rect(20, 78, 125, 20);     // PID Parameters Tab Rect
    
    rect(20, 247, 310, 100);   // RC Input Background Rect
    rect(20, 225, 125, 20);    // RC Input Tab Rect
    
    rect(335, 247, 305, 100);  // Motor Mix Background Rect
    rect(335, 225, 125, 20);   // Motor Mix Tab Rect
    
    rect(20, 394, 125, 80);    //  IMU Background Rect
    rect(20, 372, 125, 20);    //  IMU Tab Rect
    
    rect(280, 394, 60, 80);    //  PID Attitude Error Background Rect
    rect(280, 372, 60, 20);    //  PID Attitude Error Tab Rect
    
    rect(110, 125, 80, 15);    // Kp ROLL
    rect(110, 145, 80, 15);    // Kp PITCH
    rect(110, 165, 80, 15);    // Kp YAW
    
    rect(195, 125, 80, 15);    // Ki ROLL
    rect(195, 145, 80, 15);    // Ki PITCH
    rect(195, 165, 80, 15);    // Ki YAW
    
    rect(280, 125, 80, 15);    // Kd ROLL
    rect(280, 145, 80, 15);    // Kd PITCH
    rect(280, 165, 80, 15);    // Kd YAW
    
    rect(365, 135, 80, 15);    // Krate PITCH and ROLL
    rect(365, 165, 80, 15);    // Krate YAW
    
    textSize(14);
    textFont(smallBold);
    
    fill(cp5magenta);
    text("PID Parameters", 30, 93);
    text("Kp", 140, 120);
    text("Ki", 225, 120);
    text("Kd", 310, 120);
    text("RATE", 390, 120);
    
    fill(WHITE);
    text("ROLL", 30, 140);
    text("PITCH", 30, 160);
    text("YAW", 30, 180);
    
    text("THROTTLE", 30, 270);  // RC Inputs
    text("ROLL", 30, 290);
    text("PITCH", 30, 310);
    text("YAW", 30, 330);
    
    text("M4: 100%", 345, 270);  // Motor Mix
    text("M3: 100%", 530, 270);
    text("M1: 100%", 345, 310);
    text("M2: 100%", 530, 310);
    
    text("ROLL", 30, 417);  //  IMU
    text("PITCH", 30, 437);
    text("YAW", 30, 457);
    
    fill(cp5orange);  //  IMU
    text("IMU Attitude", 30, 387);
    
    fill(cp5red);  //  PID Attitude Error
    text("Error", 290, 387);
    
    stroke(WHITE);               // Drone Direction Arrow
    strokeWeight(2);
    line(488, 260, 488, 290);
    line(488, 260, 483, 270);
    line(488, 260, 493, 270);
    strokeWeight(1);
    
    if (channels[7] > SWITCH_ON) {  //  ARMED LED
      stroke(cp5red);  
      fill(cp5red); 
      text("ARMED", 240, 240); 
    }
    else {
      fill(WHITE);
      text("ARMED", 240, 240);
      stroke(WHITE);
      fill(BLACK); 
    }
    circle(300, 230, 10);       
    stroke(BLACK);
    
    fill(nexgenGold);
    text("Motor Mix", 345, 240);
    
    rect(345, 275, 100, 15);    //  M4
    rect(530, 275, 100, 15);    //  M3
    rect(345, 315, 100, 15);    //  M1
    rect(530, 315, 100, 15);    //  M2
    
    fill(nexgenAqua);
    text("RC Input", 30, 240);
    
    textSize(12);
    fill(WHITE);
    text(rollKp, 130, 136);
    text(rollKi, 215, 136);
    text(rollKd, 300, 136);
    
    text(pitchKp, 130, 156);
    text(pitchKi, 215, 156);
    text(pitchKd, 300, 156);
    
    text(yawKp, 130, 176);
    text(yawKi, 215, 176);
    text(yawKd, 300, 176);
    
    text(rollPitchRate, 390, 146);
    text(yawRate, 390, 176);
    
    //  IMU Attitude values
    String imuRoll = nfs(roll, 0, 1) + "°";
    String imuPitch = nfs(pitch, 0, 1) + "°";
    String imuYaw = nfs(yaw, 0, 1) + "°";
    
    fill(nexgenGreen);
    text(imuRoll, 100, 417);
    text(imuPitch, 100, 437);
    text(imuYaw, 100, 457);
    
    //  PID Attitude Errors
    String rollError = nfs(0.0, 0, 1) + "°";
    String pitchError = nfs(0.0, 0, 1) + "°";
    String yawError = nfs(0.0, 0, 1) + "°";
    
    fill(cp5red);
    text(rollError, 295, 417);
    text(pitchError, 295, 437);
    text(yawError, 295, 457);
      
    //  RC Input Conversions
    float rcThrottle = map(float(channels[0]), 172, 1811, 0, 150);  // Throttle = SBUS channel 0
    float rcRoll = map(float(channels[1]), 172, 1811, 0, 150);      // Roll     = SBUS channel 1
    float rcPitch = map(float(channels[2]), 172, 1811, 0, 150);     // Pitch    = SBUS channel 2
    float rcYaw = map(float(channels[3]), 172, 1811, 0, 150);       // Yaw      = SBUS channel 3
    
    float throttlePercentage = (rcThrottle / 150.0) * 100.0;
    float rollPercentage = ((rcRoll - 75.0) / 150.0) * 100.0;
    float pitchPercentage = ((rcPitch - 75.0) / 150.0) * 100.0;
    float yawPercentage = ((rcYaw - 75.0) / 150.0) * 100.0;
    
    String throttleString = int(throttlePercentage) + "%";
    String rollString = int(rollPercentage) + "°";
    String pitchString = int(pitchPercentage) + "°";
    String yawString = int(yawPercentage) + "°";
    
    fill(nexgenAqua);
    text(throttleString, 125, 270);
    text(rollString, 125, 290);
    text(pitchString, 125, 310);
    text(yawString, 125, 330);
    
    rect(165, 255, rcThrottle, 15);  //  Bar graphs
    rect(165, 275, rcRoll, 15);
    rect(165, 295, rcPitch, 15);
    rect(165, 315, rcYaw, 15);
    
    //  PID Flow Diagram Variables
    textSize(16);
    text(throttleString, 365, 614);
    
    stroke(WHITE);
    line(240, 275, 240, 290);
    line(240, 295, 240, 310);
    line(240, 315, 240, 330);
    
    drawLoopFrequency(543, 555);
    drawFrameRate(543, 640);
    
    popMatrix();
    break;
  default:
    logConfigurator("Nexgen: undefined state: " + state);
    break;
  }
}

/******************************************************************
 Handle Serial Events
 ******************************************************************/
 
void processSBUS(String payload) {
  String items[] = split(payload, '/');

  sbusAvailable = true;

  if (items.length > 1) {
    //  Standard SBUS Information
    channels[0] = int(items[0]);  // Throttle
    channels[1] = int(items[1]);  // Roll
    channels[2] = int(items[2]);  // Pitch
    channels[3] = int(items[3]);  // Yaw
    channels[4] = int(items[4]);  // Buzzer Switch
    channels[5] = int(items[5]);  // RSSI
    channels[6] = int(items[6]);  // Rx Battery Voltage
    channels[7] = int(items[7]);  // ARM / DISARM
    channels[8] = int(items[8]);  // Flight Mode
    channels[9] = int(items[9]);  // Lost Frames

    float tempRSSI = (channels[5]/1811.0) * 100.0;

    rssi = int(tempRSSI);
    rxVoltage = (channels[6]/1811.0) * 5.0;
  }
}

void processArmingFlags(int payload) {
  armPreventionFlags = payload;
  noGyroFlag = boolean(armPreventionFlags & NO_GYRO_MASK);
  failSafeFlag = boolean(armPreventionFlags & FAILSAFE_MASK);
  rxLossFlag = boolean(armPreventionFlags & RX_LOSS_MASK);
  throttleFlag = boolean(armPreventionFlags & THROTTLE_MASK);
  angleFlag = boolean(armPreventionFlags & ANGLE_MASK);
  bootFlag = boolean(armPreventionFlags & BOOT_MASK);
  lowBatFlag = boolean(armPreventionFlags & LOW_BAT_MASK);
  configFlag = boolean(armPreventionFlags & CONFIG_MASK);
  
  logConfigurator("> Arm Prevention Flags: " + Integer.toBinaryString(armPreventionFlags));
}

void resetIMUvariables() {
  imuSampleCount = 0;
  
  gyr_x = 0;
  gyr_y = 0;
  gyr_z = 0;
  acc_x = 0;
  acc_y = 0;
  acc_z = 0;
  mag_x = 0;
  mag_y = 0;
  mag_z = 0;
  
  gyr_min_x = 0;
  gyr_min_y = 0;
  gyr_min_z = 0;
  acc_min_x = 0;
  acc_min_y = 0;
  acc_min_z = 0;
  mag_min_x = 0;
  mag_min_y = 0;
  mag_min_z = 0;
  
  gyr_avg_x = 0;
  gyr_avg_y = 0;
  gyr_avg_z = 0;
  acc_avg_x = 0;
  acc_avg_y = 0;
  acc_avg_z = 0;
  mag_avg_x = 0;
  mag_avg_y = 0;
  mag_avg_z = 0;
  
  gyr_max_x = 0;
  gyr_max_y = 0;
  gyr_max_z = 0;
  acc_max_x = 0;
  acc_max_y = 0;
  acc_max_z = 0;
  mag_max_x = 0;
  mag_max_y = 0;
  mag_max_z = 0;
}

boolean parseString(String str) {
  if (str.charAt(0) == START_BYTE && str.charAt(str.length()-3) == STOP_BYTE) {
    //  Received a command response from Drone 
    String payload = str.substring(2, str.length()-3);

    switch (str.charAt(1)) {
    case 'V':
    case 'v': 
      logConfigurator("Nexgen: Configurator Version " + VERSION);
      logConfigurator("Nexgen: Magpie Firmware " + float(payload));
      break;
    case 'B':
      droneBattery = float(payload);
      logConfigurator("Nexgen: Battery voltage received: " + droneBattery);
      break;
    case 'C':
      String imuOffsets[] = split(payload, '/');
      //  DEBUG - logConfigurator("Nexgen: IMU Offset Bias Data: " + payload);
      
      if (imuOffsets.length == 9) {
        gx_bias = imuOffsets[0];
        gy_bias = imuOffsets[1];
        gz_bias = imuOffsets[2];
        ax_bias = imuOffsets[3];
        ay_bias = imuOffsets[4];
        az_bias = imuOffsets[5];
        mx_bias = imuOffsets[6];
        my_bias = imuOffsets[7];
        mz_bias = imuOffsets[8];
      }
      break;
    case 'D':
      processSBUS(payload);
      break;
    case 'F':
      loopFrequency = int(payload);
      logConfigurator("Nexgen: Loop frequency received: " + loopFrequency + " Hz");
      break;
    case 'G':
      int stateValue = int(payload);
      
      droneState = DroneStates.valueOfInt(stateValue);
      logConfigurator("Nexgen: New Drone state received: " + droneState.nameAsString());
      break;
    case 'I':
      String imu[] = split(payload, '/');
      //  DEBUG - logConfigurator("Nexgen: New IMU Data: " + payload);

      if (imu.length == 3 || imu.length == 9) {
        roll = float(imu[0]);
        pitch = float(imu[1]);
        yaw = float(imu[2]);
      }
      break;
    case 'J':
      break;
    case 'K':
      String combinedIMU[] = split(payload, '/');
      //  DEBUG - logConfigurator("Nexgen: New IMU Data: " + payload);

      if (combinedIMU.length == 12) {
        gyr_x = float(combinedIMU[0]);
        gyr_y = float(combinedIMU[1]);
        gyr_z = float(combinedIMU[2]);
        acc_x = float(combinedIMU[3]);
        acc_y = float(combinedIMU[4]);
        acc_z = float(combinedIMU[5]);
        mag_x = float(combinedIMU[6]);
        mag_y = float(combinedIMU[7]);
        mag_z = float(combinedIMU[8]);
        roll = float(combinedIMU[9]);
        pitch = float(combinedIMU[10]);
        yaw = float(combinedIMU[11]);
      }
      break;
    case 'S':
      processSBUS(payload);
      break;
    case 'R':
      float tempRSSI = (float(payload)/1811.0) * 100.0;

      rssi = int(tempRSSI);
      logConfigurator("Nexgen: RSSI received: " + rssi);
      break;
    case 'W':
      //processArmingFlags(payload);
      break;
    case 'Y':
      String imuSampleRates[] = split(payload, '/');
      
      if (imuSampleRates.length == 3) {
        gyroSampleRate = float(imuSampleRates[0]);
        accelSampleRate = float(imuSampleRates[1]);
        magSampleRate = float(imuSampleRates[2]);
      }
      break;
    case '8':
      rxVoltage = (channels[6]/1811.0) * 5.0;
      logConfigurator("Nexgen: Receiver voltage received: " + rxVoltage);
      break;
    default:
      logConfigurator("Nexgen: Unknown command received: " + str);
      logConfigurator("Nexgen: START BYTE - " + str.charAt(0) + " STOP BYTE - " + str.charAt(str.length()-3));
    }
    return true;
  }
  return false;
}

void serialEvent(Serial s) {
  List<Character> payload;
  int c;
  
  try {
    while (s.available() > 0) {
      c = (s.read());
      String hexValue = String.format("%1$02X", c);
      char ascii = (char)c;
      
      switch(c) {
       case 0: 
         ascii = '0'; // NULL
         break;
       default:
         if (c < 32 || c > 126) //  Non-printable char
           ascii = '.';
      }

      if (c_state == IDLE) {
        c_state = (c=='$') ? HEADER_START : IDLE;
      } 
      else if (c_state == HEADER_START) {
        c_state = (c=='M') ? HEADER_M : IDLE;
      } 
      else if (c_state == HEADER_M) {
        if (c == '>') {
          c_state = HEADER_ARROW;
        } 
        else if (c == '!') {
          c_state = HEADER_ERR;
        } 
        else {
          c_state = IDLE;
        }
      } 
      else if (c_state == HEADER_ARROW || c_state == HEADER_ERR) {
        /* is this an error message? */
        err_rcvd = (c_state == HEADER_ERR);        /* now we are expecting the payload size */
        dataSize = (c&0xFF);
        /* reset index variables */
        p = 0;
        offset = 0;
        checksum = 0;
        checksum ^= (c&0xFF);
        /* the command is to follow */
        c_state = HEADER_SIZE;
      } 
      else if (c_state == HEADER_SIZE) {
        cmd = (byte)(c&0xFF);
        checksum ^= (c&0xFF);
        c_state = HEADER_CMD;
      } 
      else if (c_state == HEADER_CMD && offset < dataSize) {
          checksum ^= (c&0xFF);
          inBuf[offset++] = (byte)(c&0xFF);
      } 
      else if (c_state == HEADER_CMD && offset >= dataSize) {
        /* compare calculated and transferred checksum */
        if ((checksum&0xFF) == (c&0xFF)) {
          if (err_rcvd) {
            logConfigurator("! Unrecognised request: " + c);
            //System.err.println("Copter did not understand request type "+c);
          } 
          else {
            /* we got a valid response packet, evaluate it */
            evaluateCommand(cmd, (int)dataSize);
          }
        } 
        else {
          System.out.println("invalid checksum for command "+((int)(cmd&0xFF))+": "+(checksum&0xFF)+" expected, got "+(int)(c&0xFF));
          System.out.print("<"+(cmd&0xFF)+" "+(dataSize&0xFF)+"> {");
          for (int i=0; i<dataSize; i++) {
            if (i!=0) { System.err.print(' '); }
            System.out.print((inBuf[i] & 0xFF));
          }
          System.out.println("} ["+c+"]");
          System.out.println(new String(inBuf, 0, dataSize));
        }
        c_state = IDLE;
      }
    }
  }
  catch(Exception e) {
    logError("Error reading serial port " + portName);
    //  e.printStackTrace();
  }
}

/******************************************************************
 Handle Control Events - Main Menu
 ******************************************************************/

public void mainMenu(int n) {
  switch (state) {
  case MAIN_MENU:
    break;
  case CALIBRATE_IMU:
    cp5calibrateIMU.hide();
    firstIMUread = true;
    resetIMUvariables();
    if (serialPort != null) {
      sendRequestMSP(requestMSP(MSP_STOP_STREAM));
      logConfigurator("< MSP_STOP_STREAM Request Sent");
    }
    break;
  case TEST_IMU:
    cp5testIMU.hide();
    if (serialPort != null) {
      sendRequestMSP(requestMSP(MSP_STOP_STREAM));
      logConfigurator("< MSP_STOP_STREAM Request Sent");
    }
    break;
  case RADIO_CONTROL:
    cp5radioControl.hide();
    sbusAvailable = false;
    if (serialPort != null) {
      sendRequestMSP(requestMSP(MSP_STOP_STREAM));
      logConfigurator("< MSP_STOP_STREAM Request Sent");
    }
    break;
  case CALIBRATE_ESC:
    cp5calibrateESC.hide();
    enterMotors = true;
    nextButton.show();
    calibrationState = CalibrationState.START;
    disarmMotors();
    break;
  case TEST_MOTORS:
    cp5testMotors.hide();
    if (mc.armed) { logConfigurator("Nexgen: Warning - motors still armed."); }
    enterMotors = true;
    break;
  case TUNE_PID:
    cp5tunePID.hide();
    sbusAvailable = false;
    if (serialPort != null) {
      sendRequestMSP(requestMSP(MSP_STOP_STREAM));
      logConfigurator("< MSP_STOP_STREAM Request Sent");
    }
    break;
  }

  cp5mainMenu.show();
  state = AppState.MAIN_MENU;
  
  if (serialPort != null) {
    sendRequestMSP(requestMSP(MSP_ANALOG));
    sendRequestMSP(requestMSP(MSP_STATUS));
    logConfigurator("< MSP_ANALOG Request Sent");
    logConfigurator("< MSP_STATUS Request Sent");
  }
}

public void calibrateIMU(int n) {
  state = AppState.CALIBRATE_IMU;
  cp5mainMenu.hide();
  cp5calibrateIMU.show();
  
  //  Request IMU streamed on serial from Drone
  if (serialPort != null) {
    sendRequestMSP(requestMSP(MSP_RAW_IMU));
    sendRequestMSP(requestMSP(MSP_IMU_ODR));
    sendRequestMSP(requestMSP(MSP_IMU_BIAS));
    logConfigurator("< MSP_RAW_IMU Request Sent");
    logConfigurator("< MSP_IMU_ODR Request Sent");
    logConfigurator("< MSP_IMU_BIAS Request Sent");
  } else {
    logError("Nexgen: Can't stream IMU calibration data - not connected");
  }
}

public void calibrateCode(int n) {
  //  Commence IMU calibration
  if (serialPort != null) {
    sendRequestMSP(requestMSP(MSP_IMU_CALIBRATION));
    logConfigurator("< MSP_IMU_CALIBRATION Request Sent");
  } else {
    logError("Nexgen: Can't calibrate IMU - not connected");
  }
}

public void testIMU(int n) {
  state = AppState.TEST_IMU;
  cp5mainMenu.hide();
  cp5testIMU.show();

  //  Request IMU streamed on serial from Drone
  if (serialPort != null) {
    sendRequestMSP(requestMSP(MSP_ATTITUDE));
    sendRequestMSP(requestMSP(MSP_RAW_IMU));
    logConfigurator("< MSP_ATTITUDE Request Sent");
    logConfigurator("< MSP_RAW_IMU Request Sent");
  } else {
    logError("Nexgen: Can't stream IMU test data - not connected");
  }
}

public void sliderIMUscale(int n) {
  float scalePercentage = float(n) / 100.0;
  int chartHeight = int(200.0 * scalePercentage);
  
  //  logConfigurator("Nexgen: IMU Chart Scale: " + n + "%");
  imuChart.setSize(600, chartHeight);
  imuDataList.setPosition(240, 105 + chartHeight);
}

public void imuData(int n) {
  imuStreamOption = IMUStreamOptions.valueOfInt(n);
  logConfigurator("Nexgen: Selected IMU Filter: " + imuStreamOption.nameAsString());
  
  //  Request IMU data streamed on serial from Drone
  if (serialPort != null) {
    //serialPort.write(imuStreamOption.apiCodeString());
    logConfigurator("Nexgen: To DO - Change sensor fusion filter");
  } else {
    logError("Nexgen: Can't stream IMU - not connected");
  }
}

public void resetIMU(int n) {
  //  Request drone to zero out angle calculations
  if (serialPort != null) {
    sendRequestMSP(requestMSP(MSP_ANALOG));
    sendRequestMSP(requestMSP(MSP_STATUS));
    logConfigurator("< MSP_ANALOG Request Sent");
    logConfigurator("< MSP_STATUS Request Sent");
  } else {
    logError("Nexgen: Can't reset IMU - not connected");
  }
}

public void requestLoopFreq(int n) {
  //  Request loop frequency update from Drone
  if (serialPort != null) {
    sendRequestMSP(requestMSP(MSP_ANALOG));
    sendRequestMSP(requestMSP(MSP_STATUS));
    logConfigurator("< MSP_ANALOG Request Sent");
    logConfigurator("< MSP_STATUS Request Sent");
  } else {
    logError("Nexgen: Can't request loop frequency - not connected");
  }
}

public void configDisarm(int n) {
  disarmMotors();
}

public void radioControl(int n) {
  state = AppState.RADIO_CONTROL;
  cp5mainMenu.hide();
  cp5radioControl.show();

  //  Request SBUS streamed on serial from Drone
  if (serialPort != null) {
    sendRequestMSP(requestMSP(MSP_RC));
    sendRequestMSP(requestMSP(MSP_ANALOG));
    sendRequestMSP(requestMSP(MSP_STATUS));
    logConfigurator("< MSP_RC Request Sent");
    logConfigurator("< MSP_ANALOG Request Sent");
    logConfigurator("< MSP_STATUS Request Sent");
  } else {
    logError("Nexgen: Can't stream SBUS - not connected");
  }
}

public void nextStep(int n) {
  switch(calibrationState) {
  case START:
    if (droneBattery < 5.0) {
      calibrationState = CalibrationState.CONNECT;
    } else {
      logError("Nexgen: Disconnect LiPo before proceeding.");
    }
    break;
  case CONNECT:
    if (serialPort != null) {
      calibrationState = CalibrationState.ARM;
    }
    break;
  case ARM:
    if (droneBattery < 5.0 && mc.armed) {
      calibrationState = CalibrationState.MAX_THROTTLE;
      masterCalibrateThrottle.setValue(100);
      logConfigurator("Nexgen: All motors maximum throttle");
    } else {
      logError("Nexgen: Disconnect LiPo and arm motors before proceeding.");
    }
    break;
  case MAX_THROTTLE:
    calibrationState = CalibrationState.MIN_THROTTLE;
    masterCalibrateThrottle.setValue(0);
    logConfigurator("Nexgen: All motors minimum throttle");
    break;
  case MIN_THROTTLE:
    calibrationState = CalibrationState.DONE;
    nextButton.hide();
    break;
  case DONE:
    break;
  }
}

public void calibrateESC(int n) {
  state = AppState.CALIBRATE_ESC;
  cp5mainMenu.hide();
  cp5calibrateESC.show();
  if (serialPort != null) {
    serialPort.write("<E>");
    logConfigurator("Nexgen: Commencing Calibration");
  } else {
    logError("Nexgen: Can't calibrate ESC - not connected");
  }
}

public void testMotors(int n) {
  state = AppState.TEST_MOTORS;
  cp5mainMenu.hide();
  cp5testMotors.show();
}

public void tunePID(int n) {
  state = AppState.TUNE_PID;
  cp5mainMenu.hide();
  cp5tunePID.show();
  
  //  Request SBUS streamed on serial from Drone
  if (serialPort != null) {
    serialPort.write("<D>");
    logConfigurator("Nexgen: Requested PID Streaming");
  } else {
    logError("Nexgen: Can't stream PID - not connected");
  }
}

public String charArrayToString(Character[] chars) {
  StringBuilder sb = new StringBuilder(chars.length);
  
  for (Character c : chars)
    sb.append(c.charValue());
    
  return sb.toString();
}

public void disarmMotors() {
  mc.armed = false;
  mc.throttle = 0;
  mc.throttle1 = 0;
  mc.throttle2 = 0;
  mc.throttle3 = 0;
  mc.throttle4 = 0;
  mc.throttleOffset = 0;
  
  if (serialPort != null) {
    sendRequestMSP(requestMSP(MSP_SET_DISARM));
    logConfigurator("< MSP_SET_DISARM Request Sent");
  } else if (enterMotors) {
    logError("Nexgen: Can't disarm motors - not connected");
    enterMotors = false;
  }

  if (state == AppState.TEST_MOTORS || state == AppState.MAIN_MENU) {
    armTestToggle.setBroadcast(false);
    armTestToggle.setValue(false);
    armTestToggle.setCaptionLabel("Motors Disarmed - Click to arm");
    armTestToggle.setColorLabel(color(255));
    armTestToggle.setBroadcast(true);

    masterTestThrottle.setValue(0);
    masterTestThrottle.setLock(true);

    m1dutyCycle.setValue(0);
    m1dutyCycle.setLock(true);
    m2dutyCycle.setValue(0);
    m2dutyCycle.setLock(true);
    m3dutyCycle.setValue(0);
    m3dutyCycle.setLock(true);
    m4dutyCycle.setValue(0);
    m4dutyCycle.setLock(true);
  } else if (state == AppState.CALIBRATE_ESC) {
    masterCalibrateThrottle.setValue(0);
    masterCalibrateThrottle.setLock(true);

    armCalibrateToggle.setBroadcast(false);
    armCalibrateToggle.setValue(false);
    armCalibrateToggle.setCaptionLabel("Motors Disarmed - Click to arm");
    armCalibrateToggle.setColorLabel(color(255));
    armCalibrateToggle.setBroadcast(true);
  }
}

public void armMotors(int n) {

  Toggle armToggle;

  if (state == AppState.TEST_MOTORS) {
    armToggle = armTestToggle;
    masterTestThrottle.setValue(0.5);
    masterTestThrottle.setLock(false);
  } else {
    armToggle = armCalibrateToggle;
    masterCalibrateThrottle.setValue(0.5);
    masterCalibrateThrottle.setLock(false);
  }

  if (armToggle.getState()) {
    if (serialPort != null) {
      sendRequestMSP(requestMSP(MSP_SET_ARM));
      logConfigurator("< MSP_SET_ARM Request Sent");
    } else if (enterMotors) {
      logError("Nexgen: Can't arm motors - not connected");
      enterMotors = false;
    }
    
    armToggle.setCaptionLabel("Motors Armed - Click to disarm");
    armToggle.setColorLabel(cp5red);

    mc.armed = true;
    mc.throttle1 = 0.5;
    mc.throttle2 = 0.5;
    mc.throttle3 = 0.5;
    mc.throttle4 = 0.5;
    mc.throttleOffset = 0.02;

    if (state == AppState.TEST_MOTORS) {
      m1dutyCycle.setValue(0.5);
      m1dutyCycle.setLock(false);
      m2dutyCycle.setValue(0.5);
      m2dutyCycle.setLock(false);
      m3dutyCycle.setValue(0.5);
      m3dutyCycle.setLock(false);
      m4dutyCycle.setValue(0.5);
      m4dutyCycle.setLock(false);
    }
  } else {
    disarmMotors();
  }
}

public void sendMotorControlPacket(char target, int speed) {
  //  Arduino is expecting a pulse width between 1000 us and 2000 us
  //  MIN_ASCII to MAX_ASCII is the encoded motor speed 0 - 100%
  //  Character[] charArrayPayload = {MIN_ASCII, MIN_ASCII, MIN_ASCII, MIN_ASCII};  
    
  //sendRequestMSP(requestMSP (MSP_SET_MOTOR, charArrayPayload));
  //sendRequestMSP(requestMSP(MSP_SET_DISARM));
  //logConfigurator("< MSP_SET_MOTOR Request Sent");
  //logConfigurator("DEBUG: DISARM Payload = " + charArrayToString(charArrayPayload));
  
  float dutyCycle = float(speed) / 100.0;
  int pulseWidth = int(dutyCycle * MIN_PULSEWIDTH + MIN_PULSEWIDTH);
  
  pulseWidth = constrain(pulseWidth, MIN_PULSEWIDTH, MAX_PULSEWIDTH);
  
  String controlMSG = "<" + target + pulseWidth + ">";
  
  if (serialPort != null) {
    serialPort.write(controlMSG);
  } else if (enterMotors) {
    logError("Nexgen: Can't control motors - not connected");
    enterMotors = false;
  }
}

public void masterThrottleKnob(int n) {
  mc.setThrottle(n);
  updateMotorDutyCycle();
  sendMotorControlPacket('A', n);
}

public void sliderM1(int n) {
  String sliderLabel = "        M1 (CCW) Duty Cycle: " + n + "%";

  m1dutyCycle.setCaptionLabel(sliderLabel);
  mc.throttle1 = n;
  sendMotorControlPacket('1', n);
}

public void sliderM2(int n) {
  String sliderLabel = "        M2 (CW) Duty Cycle: " + n + "%";

  m2dutyCycle.setCaptionLabel(sliderLabel);
  mc.throttle2 = n;
  sendMotorControlPacket('2', n);
}

public void sliderM3(int n) {
  String sliderLabel = "        M3 (CCW) Duty Cycle: " + n + "%";

  m3dutyCycle.setCaptionLabel(sliderLabel);
  mc.throttle3 = n;
  sendMotorControlPacket('3', n);
}

public void sliderM4(int n) {
  String sliderLabel = "        M4 (CW) Duty Cycle: " + n + "%";

  m4dutyCycle.setCaptionLabel(sliderLabel);
  mc.throttle4 = n;
  sendMotorControlPacket('4', n);
}

public void droneType(int n) {
  //  New drone variety selected - TODO
}

public void imuType(int n) {
  //  logConfigurator("Nexgen: DEBUG - IMU Selected: " + n);
  if (n == 0) { 
    selectedIMU = IMUType.LSM9DS1;
  }
  else {
    selectedIMU = IMUType.MPU6500;
  }
}

/******************************************************************
 Handle Control Events - Serial
 ******************************************************************/

public void disconnectSerial(int n) {
  if (serialPort != null) {
    sendRequestMSP(requestMSP(MSP_CONFIG_CLOSE));
    logConfigurator("< MSP_CONFIG_CLOSE Request Sent");
    serialPort.clear();
    serialPort.stop();
    serialPort = null;
    logConfigurator("Nexgen: " + portName + " disconnected.");
    logConfigurator("________________________________________\n");
    serialPortsList.setCaptionLabel("Serial Ports");
    serialPortsList.setColorBackground(darkGrey);
    discButton.setColorBackground(darkGrey);
    droneBattery = 0.0;
    rxVoltage = 0.0;
    rssi = 0;
    sbusAvailable = false;
    armPreventionFlags = 0;
    processArmingFlags(0);
    droneState = DroneStates.DISCONNECTED;
  }
}

public void refreshSerial(int n) {
  String[] portNames = Serial.list();

  serialPortsList.clear();

  for (int i = 0; i < portNames.length; i++) 
    serialPortsList.addItem(portNames[i], i);

  serialPortsList.close();
  serialPortsList.setCaptionLabel("Serial Ports");
  logConfigurator("Serial port list refreshed");
}

public void clearConsole(int n) {
  consoleText.clear();
}

public void copyConsole(int n) {
  GClip.copy(consoleText.getText());
}

public void serialPorts(int n) {
  //check if there's a serial port open already, if so, close it
  if (serialPort != null) {
    serialPort.clear();
    serialPort.stop();
    serialPort = null;
  }

  //open the selected port
  portName = cp5console.get(ScrollableList.class, "serialPorts").getItem(n).get("name").toString();

  try {
    serialPort = new Serial(this, portName, BAUD_RATE);
    serialPortsList.setColorBackground(darkGreen);
    logConfigurator("Connected to Serial Port: " + portName);
    logConfigurator("========================================\n");
    discButton.setColorBackground(cp5red);
    
    //  Request IDENT
    sendRequestMSP(requestMSP(MSP_IDENT));
    logConfigurator("< MSP_IDENT Request Sent");
    
    //  Request VARIANT
    sendRequestMSP(requestMSP(MSP_FC_VARIANT));
    logConfigurator("< MSP_FC_VARIANT Request Sent");
    
    //  Request FC VERSION
    sendRequestMSP(requestMSP(MSP_FC_VERSION));
    logConfigurator("< MSP_FC_VERSION Request Sent");

    //  Request Battery Voltage, RSSI and Receiver Voltage from Drone
    sendRequestMSP(requestMSP(MSP_ANALOG));
    logConfigurator("< MSP_ANALOG Request Sent");
    
    delay(500);
    
    sendRequestMSP(requestMSP(MSP_STATUS));
    logConfigurator("< MSP_STATUS Request Sent");

    //  Initialise ESC calibration if required
    if (state == AppState.CALIBRATE_ESC) {
      
      logConfigurator("Nexgen: Commencing Calibration");
    }
  }
  catch(Exception e) {
    logError("Error opening serial port " + portName);
    //e.printStackTrace();
  }
}

/******************************************************************
 KEYBOARD HANDLER
 ******************************************************************/

void keyPressed() {
  if (key == CODED) {
    switch (keyCode) {
    case UP:     
      controlState = ControlStates.INC_PITCH; 
      break; 
    case DOWN:   
      controlState = ControlStates.DEC_PITCH; 
      break;
    case LEFT:   
      controlState = ControlStates.INC_ROLL; 
      break;
    case RIGHT:  
      controlState = ControlStates.DEC_ROLL; 
      break;
    }
  } else {

    controlState = ControlStates.valueOfCode(key);

    if (controlState == null) {
      controlState = ControlStates.NOP;
      logError("Nexgen: Unknown command - " + key);
    }

    switch (key) {
    case 'b':
    case 'B':
      if (serialPort != null) {
        serialPort.write("<B>");
        logConfigurator("Nexgen: Requested battery voltage");
      } else {
        logError("Nexgen: No serial connection. Command <B> unavailable.");
      }
      break;
    case 'f':
    case 'F':
      if (serialPort != null) {
        serialPort.write("<F>");
        logConfigurator("Nexgen: Requested Flight Code Loop Frequency");
      } else {
        logError("Nexgen: No serial connection. Command <F> unavailable.");
      }
      break;
    case 'h':
    case 'H':
      logHelp("\nNexgen: Valid Commands - Arrow Keys for pitch and roll, wasd for throttle and yaw, v = version, f = loop frequency, b = read LiPo voltage, h = help, space = reset position.");
      logHelp("\nOn Test IMU screen - 0 = deselect all, 1 = toggle roll, 2 = toggle pitch, and 3 = toggle yaw.\n");
      logHelp("\nOn Calibrate IMU screen - y = request IMU sample rates.\n");
      break;
    case 'p':
    case 'P':
      if (serialPort != null) {
        serialPort.write("<P>");
        logConfigurator("Nexgen: Requested Hardware Pin Configuration");
      } else {
        logError("Nexgen: No serial connection. Command <P> unavailable.");
      }
      break;
    case 'v':
    case 'V': 
      if (serialPort != null) {
        serialPort.write("<V>");
        logConfigurator("Nexgen: Requested Magpie firmware version");
      } else {
        logError("Nexgen: No serial connection. Command <V> unavailable.");
      }
      break;
    case 'y':
    case 'Y':
      if (serialPort != null) {
        serialPort.write("<Y>");
        logConfigurator("Nexgen: Requested IMU Sample Rates");
      } else {
        logError("Nexgen: No serial connection. Command <Y> unavailable.");
      }
      break;
    }
  }

  if (state == AppState.TEST_IMU) {
    switch (key) {
    case '0': 
      imuSelect.deactivateAll(); 
      break;
    case '1': 
      imuSelect.toggle(0); 
      break;
    case '2': 
      imuSelect.toggle(1); 
      break;
    case '3': 
      imuSelect.toggle(2); 
      break;
    }
  }
}

void keyReleased() {
  controlState = ControlStates.NOP;
}
