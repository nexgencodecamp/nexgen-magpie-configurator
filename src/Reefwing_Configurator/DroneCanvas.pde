/******************************************************************
 DroneCanvas, is the Canvas render class for the simulated drone
 ******************************************************************/

public class DroneCanvas extends Canvas {

  public float roll, pitch, yaw, throttle;
  public boolean motorsOnly = false;
  public boolean armed = false;
  public float angle1 = 0, angle2 = 0, angle3 = 0, angle4 = 0;
  public float throttle1, throttle2, throttle3, throttle4;

  public DroneCanvas() {
    roll = 0.0;
    pitch = 0.0;
    yaw = 0.0;
    throttle = 0.0;
  }

  float throttleOffset = 0.2;
  PImage textureMap;
  PShape body, arms, motor, propGuards, props;
  color nexgenGold = color(203, 197, 150);
  PVector b, a, pg1, pg2, pg3, pg4;
  Propeller prop;

  /******************************************************************
   Create Cylinders (can has end caps)
   ******************************************************************/

  PShape cylinder(float r, float h, int detail) {

    textureMode(NORMAL);
    PShape sh = createShape();

    sh.beginShape(QUAD_STRIP);
    sh.noStroke();
    for (int i = 0; i <= detail; i++) {
      float angle = TWO_PI / detail;
      float x = sin(i * angle);
      float z = cos(i * angle);
      float u = float(i) / detail;
      sh.normal(x, 0, z);
      sh.vertex(x * r, -h/2, z * r, u, 0);
      sh.vertex(x * r, +h/2, z * r, u, 1);
    }
    sh.endShape();

    return sh;
  }

  PShape can(int sides, float r, float h) {

    PShape cylinder = createShape(GROUP);

    float angle = 360 / sides;
    float halfHeight = h / 2;

    // draw top of the tube
    PShape top = createShape();
    top.beginShape();
    for (int i = 0; i < sides; i++) {
      float x = cos( radians( i * angle ) ) * r;
      float y = sin( radians( i * angle ) ) * r;
      top.vertex( x, y, -halfHeight);
    }
    top.endShape(CLOSE);
    cylinder.addChild(top);

    // draw bottom of the tube
    PShape bottom = createShape();
    bottom.beginShape();
    for (int i = 0; i < sides; i++) {
      float x = cos( radians( i * angle ) ) * r;
      float y = sin( radians( i * angle ) ) * r;
      bottom.vertex( x, y, halfHeight);
    }
    bottom.endShape(CLOSE);
    cylinder.addChild(bottom);

    // draw sides
    PShape middle = createShape();
    middle.beginShape(TRIANGLE_STRIP);
    for (int i = 0; i < sides + 1; i++) {
      float x = cos( radians( i * angle ) ) * r;
      float y = sin( radians( i * angle ) ) * r;
      middle.vertex( x, y, halfHeight);
      middle.vertex( x, y, -halfHeight);
    }
    middle.endShape(CLOSE);
    cylinder.addChild(middle);

    return cylinder;
  }

  /******************************************************************
   Public Control Methods
   ******************************************************************/

  public float getThrottle() {
    return throttle;
  }
  
  public float maxThrottle() {
    float[] throttleList = {throttle1, throttle2, throttle3, throttle4};
    
    return max(throttleList);
  }

  public void incrementYaw(float value) {
    yaw += value;
  }

  public void incrementPitch(float value) {
    pitch += value;
  }

  public void incrementRoll(float value) {
    roll += value;
  }
  
  public void setThrottle(float value) {
    throttle = value;
    throttle1 = value;
    throttle2 = value;
    throttle3 = value;
    throttle4 = value;
  }

  public void incrementThrottle(float value) {
    //  clamp value between 0 and 100
    if (armed) {
      if (motorsOnly) {
        throttle1 = constrain(throttle1 + value, 0, 100);
        throttle2 = constrain(throttle2 + value, 0, 100);
        throttle3 = constrain(throttle3 + value, 0, 100);
        throttle4 = constrain(throttle4 + value, 0, 100);
      } else {
        throttle = constrain(throttle + value, 0, 100);
      }
    }
  }

  /******************************************************************
   SETUP
   ******************************************************************/

  public void setup(PGraphics pg) {
    noFill();
    noStroke();
    b = new PVector(width/2, height/2, 0);
    a = b.copy();

    a.x = 0;
    a.y -= 350;

    pg1 = b.copy();
    pg1.x -= 262;
    pg1.y -= 350;
    pg1.z += 250;

    pg2 = b.copy();
    pg2.x -= 262;
    pg2.y -= 350;
    pg2.z -= 250;

    pg3 = b.copy();
    pg3.x -= 762;
    pg3.y -= 350;
    pg3.z -= 250;

    pg4 = b.copy();
    pg4.x -= 762;
    pg4.y -= 350;
    pg4.z += 250;

    textureMap = loadImage("shiny_hull.png");
    body = createShape(SPHERE, 200); 
    body.setTexture(textureMap);

    stroke(0);
    rectMode(CORNER);
    arms = createShape(BOX, 720, 10, 40);
    arms.setTexture(textureMap);

    stroke(255);
    fill(255);

    prop = new Propeller(300, 50);
    props = prop.createProp();
    propGuards = cylinder(180, 50, 32);
    propGuards.setTexture(textureMap);

    noStroke();
    motor = can(30, 20, 25);
    motor.setTexture(textureMap);
  }

  /******************************************************************
   DRAW
   ******************************************************************/

  public void draw(PGraphics pg) {
    pushMatrix();
    //translate(b.x, b.y, b.z); 
    if (motorsOnly) {
      scale(0.5);
      translate(600, 700, 0);
      rotateX(radians(90));
      rotateY(radians(90));
    } else {
      translate(width - 200, height - 200, 0);
      scale(0.25);
      rotateX(pitch);
      rotateY(yaw);
      rotateZ(roll);

      pushMatrix();
      shape(body);
      popMatrix();

      pushMatrix();
      translate(a.x, a.y, a.z);
      rotateY(radians(45));
      shape(arms);
      popMatrix();

      pushMatrix();
      translate(a.x, a.y, a.z);
      rotateY(radians(-45));
      shape(arms);
      popMatrix();
    }

    //  Motor 1 - CCW

    pushMatrix();
    translate(pg2.x, pg2.y, pg2.z);
    shape(propGuards);
    popMatrix();

    pushMatrix();
    translate(pg2.x, pg2.y, pg2.z);
    rotateY(angle1);
    shape(props);
    if (motorsOnly) {
      throttle = throttle1;
    } 
    angle1 -= throttleOffset + throttle * 0.01;
    popMatrix();

    pushMatrix();
    translate(pg2.x, pg2.y, pg2.z);
    rotateX(radians(90));
    shape(motor);
    popMatrix();

    //  Motor 2 - CW

    pushMatrix();
    translate(pg1.x, pg1.y, pg1.z);
    shape(propGuards);
    popMatrix();

    pushMatrix();
    translate(pg1.x, pg1.y, pg1.z);
    rotateY(angle2);
    shape(props);
    if (motorsOnly) {
      throttle = throttle2;
    } 
    angle2 += throttleOffset + throttle * 0.01;
    popMatrix();

    pushMatrix();
    translate(pg1.x, pg1.y, pg1.z);
    rotateX(radians(90));
    shape(motor);
    popMatrix();

    //  Motor 3 - CCW

    pushMatrix();
    translate(pg4.x, pg4.y, pg4.z);
    shape(propGuards);
    popMatrix();

    pushMatrix();
    translate(pg4.x, pg4.y, pg4.z);
    rotateY(angle3);
    shape(props);
    if (motorsOnly) {
      throttle = throttle3;
    } 
    angle3 -= throttleOffset + throttle * 0.01;
    popMatrix();

    pushMatrix();
    translate(pg4.x, pg4.y, pg4.z);
    rotateX(radians(90));
    shape(motor);
    popMatrix();

    //  Motor 4 - CW

    pushMatrix();
    translate(pg3.x, pg3.y, pg3.z);
    shape(propGuards);
    popMatrix();

    pushMatrix();
    translate(pg3.x, pg3.y, pg3.z);
    rotateY(angle4);
    shape(props);
    if (motorsOnly) {
      throttle = throttle4;
    } 
    angle4 += throttleOffset + throttle * 0.01;
    popMatrix();

    pushMatrix();
    translate(pg3.x, pg3.y, pg3.z);
    rotateX(radians(90));
    shape(motor);
    popMatrix();

    popMatrix();
  }
}

/******************************************************************
 PROPELLER CLASS
 ******************************************************************/

class Propeller {
  float propX;
  float propY;
  float propW;
  float propH;

  Propeller() {
    propX = 0;
    propY = 0;
    propW = 50;
    propH = 50;
  }

  Propeller(int w, int h) {
    propX = 0;
    propY = 0;
    propW = w;
    propH = h;
  }

  PShape createProp() {
    pushMatrix();
    textureMode(NORMAL);
    ellipseMode(CORNER);
    PShape propGroup = createShape(GROUP);
    PShape p1 = createShape(ELLIPSE, -propW/2, -propH/2, propW/2, propH);
    PShape p2 = createShape(ELLIPSE, propX, -propH/2, propW/2, propH);

    propGroup.addChild(p1);
    propGroup.addChild(p2);

    popMatrix();

    return propGroup;
  }
}
